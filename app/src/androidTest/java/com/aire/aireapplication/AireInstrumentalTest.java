package com.aire.aireapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;

import com.aire.aireapplication.repository.AireRepository;
import com.aire.aireapplication.utils.DatabaseTestLoader;
import com.aire.aireapplication.utils.Keys;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Miguel on 28/05/2018.
 */

public class AireInstrumentalTest {
    AireRepository repository;

    // regla para que se ejecute MainActivity al iniciar un test
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void beforeTest(){
        repository = new AireRepository(mActivityRule.getActivity().getApplication());
        repository.clearDatabase();
        new DatabaseTestLoader(repository).execute();

        SharedPreferences sharedPref = mActivityRule.getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Keys.PREF_SELECTED_CITY, 0);
        editor.commit();

        onView(isRoot()).perform(pressBack());
    }

    @After
    public void afterTest(){
        // limpiamos esto

        SharedPreferences sharedPref = mActivityRule.getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Keys.PREF_SELECTED_CITY, 0);
        editor.commit();

        repository.clearDatabase();
    }

    public void openDrawer(int id){
        onView(withId(id)).perform(DrawerActions.open());
    }
}
