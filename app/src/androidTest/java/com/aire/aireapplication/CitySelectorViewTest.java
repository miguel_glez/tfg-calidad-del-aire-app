package com.aire.aireapplication;

import android.support.test.filters.LargeTest;
import android.widget.AutoCompleteTextView;

import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

import static com.aire.aireapplication.utils.EspressoTestMatchers.clickChildViewWithId;
import static org.junit.Assert.*;

/**
 * Created by Miguel on 28/05/2018.
 */

@LargeTest
public class CitySelectorViewTest extends AireInstrumentalTest{

    // CS-001
    @Test
    public void citySetFavorite_isCorrect(){
        // en DatabaseTestLoader, cityB es 1001.
        // ahí sale como favorita
        assertTrue(repository.getCityAsync(1001).isFavorite());

        // debido al orden que se sigue, debería aparecer la primera, por tanto...
        onView(withId(R.id.city_selector_recycler_view))
                .perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_checkbox)));

        // debería quedar desmarcada de favorita ahora
        assertFalse( repository.getCityAsync(1001).isFavorite() );
    }

    // CS-002
    @Test
    public void citySearch_isCorrect(){
        onView(withId(R.id.toolbar_city_selector_search))
                .perform(click());

        onView(isAssignableFrom(AutoCompleteTextView.class))
                .perform(typeText("A"));

        // sólo debería haber 1
        onView(withId(R.id.city_selector_adapter_name))
                .check(matches(withText("cityA")));

    }
}
