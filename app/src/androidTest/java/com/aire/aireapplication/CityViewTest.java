package com.aire.aireapplication;

import android.support.test.espresso.contrib.RecyclerViewActions;

import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.PollutionStaticData;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.aire.aireapplication.utils.EspressoTestMatchers.clickChildViewWithId;
import static com.aire.aireapplication.utils.EspressoTestMatchers.withDrawable;
import static com.aire.aireapplication.utils.EspressoTestMatchers.withIndex;

/**
 * Created by Miguel on 28/05/2018.
 */

public class CityViewTest extends AireInstrumentalTest {
    @Before
    public void beforeTest(){
        super.beforeTest();

        // de esta forma todos los tests empezarán con la vista de la ciudad B
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));
    }

    // CV-001
    @Test
    public void basicInfoFragment_isCorrect(){
        // la ciudad B es la 1001
        List<Station> stations = repository.stationDAO.getCityStationsAsync(1001);

        // sacamos la más contaminada
        String maxPolluter = "";
        int aqi = 0;
        for (Station station: stations){
            int stationAqi = station.getLatestReading().getAirQuality().intValue();
            if (stationAqi > aqi){
                aqi = stationAqi;
                maxPolluter = station.getLatestReading().getDominentPolluter();
            }
        }
        if (maxPolluter.equals("PM25")) maxPolluter = "PM2.5";

        // vamos comprobando que los 4 elementos posibles sean acordes a la información
        // los métodos usados... ya tienen su prueba unitaria correspondiente
        onView(withId(R.id.basic_info_aqi))
                .check(matches(withText(String.valueOf(aqi))));

        onView(withId(R.id.basic_info_max_polluter))
                .check(matches(withText(maxPolluter)));

        onView(withId(R.id.basic_info_description))
                .check(matches(withText(PollutionStaticData.getDescription(aqi))));

        onView(withId(R.id.basic_info_image))
                .check(matches(withDrawable(PollutionStaticData.getPollutionImage(aqi))));
    }

    // CV-002
    @Test
    public void cityAdapter_isCorrect(){
        // la ciudad B es la 1001
        List<Station> stations = repository.stationDAO.getCityStationsAsync(1001);

        int i=0;
        for (Station station: stations){
            int stationAqi = station.getLatestReading().getAirQuality().intValue();
            String maxPolluter = station.getLatestReading().getDominentPolluter();
            if (maxPolluter.equals("PM25")) maxPolluter = "PM2.5";

            onView(withText(station.getName()))
                    .check(matches(isDisplayed()));

            onView(withIndex(withText(String.valueOf(stationAqi)), i))
                    .check(matches(isDisplayed()));

            onView(withText(String.format( "%s: %s", App.getContext().getResources().getString(R.string.max_polluter), maxPolluter)))
                    .check(matches(isDisplayed()));

            i++;
        }
    }
}
