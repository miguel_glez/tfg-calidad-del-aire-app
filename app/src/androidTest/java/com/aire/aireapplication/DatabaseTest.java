package com.aire.aireapplication;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;

import com.aire.aireapplication.persistence.database.AireDatabase;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.utils.DatabaseTestLoader;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.List;

import static junit.framework.Assert.assertEquals;

public class DatabaseTest {
    AireDatabase database;

    private Observer<List<Reading>> observer;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void beforeTest(){
        database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AireDatabase.class)
                .allowMainThreadQueries()
                .build();

        DatabaseTestLoader task = new DatabaseTestLoader(database);
        task.doInBackground();
    }


    // DB-001
    @Test
    public void getStationLastDayReadings_check(){
        observer = new Observer<List<Reading>> (){
            @Override
            public void onChanged(@Nullable List<Reading> readings) {
                if (readings != null){
                    if (readings.size() > 0) {
                        assertEquals(1, readings.size());
                    }
                }
            }
        };

        // 10001 es la B.
        // en la clase de insertar los datos, sólo añadimos una en el lapso de las últimas 24h.
        LiveData<List<Reading>> list = database.readingDAO().getStationLastDayReadings(10001);
        list.observeForever(observer);
    }

    // DB-002
    @Test
    public void getStationLastWeekReadings_check() {
        observer = new Observer<List<Reading>> (){
            @Override
            public void onChanged(@Nullable List<Reading> readings) {
                if (readings != null){
                    if (readings.size() > 0) {
                        assertEquals(2, readings.size());
                    }
                }
            }
        };

        // 10001 es la B.
        // en la clase de insertar los datos, sólo añadimos una en el lapso de las últimas 24h.
        LiveData<List<Reading>> list = database.readingDAO().getStationLastWeekReadings(10001);
        list.observeForever(observer);
    }
}
