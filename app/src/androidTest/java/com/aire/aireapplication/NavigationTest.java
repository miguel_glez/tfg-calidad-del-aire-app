package com.aire.aireapplication;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.aire.aireapplication.utils.EspressoTestMatchers.clickChildViewWithId;

import org.junit.Test;
import org.junit.runner.RunWith;
/**
 * Created by Miguel on 28/05/2018.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NavigationTest extends AireInstrumentalTest{

    // NAV-001
    @Test
    public void onLaunch_checkCitySelectorShows(){
        onView(withId(R.id.city_selector_recycler_view))
                .check(matches(isDisplayed()));
    }

    // NAV-002
    @Test
    public void onCitySelected_showCity(){
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.city_recycler_view))
                .check(matches(isDisplayed()));
    }

    // NAV-003
    @Test
    public void onMapClicked_showMap(){
        openDrawer(R.id.drawer);

        onView(withText(R.string.map))
                .perform(click());

        withText(R.string.map)
                .matches(onView(withId(R.id.toolbar)));
    }

    // NAV-004
    @Test
    public void onMarkerClicked_showStation(){
        openDrawer(R.id.drawer);

        onView(withText(R.string.map))
                .perform(click());

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        UiObject marker = device.findObject( new UiSelector().descriptionContains("stationA") );

        try {
            marker.click();
            UiObject desc = device.findObject( new UiSelector().descriptionContains( mActivityRule.getActivity().getResources().getString(R.string.map_click_to_see)));
            desc.click();

            onView(withId(R.id.station_last_hour_container))
                    .check(matches(isDisplayed()));
        } catch (Exception e){}

    }

    // NAV-005
    @Test
    public void onStationFromMapPressBack_showMap(){
        openDrawer(R.id.drawer);

        onView(withText(R.string.map))
                .perform(click());

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        UiObject marker = device.findObject( new UiSelector().descriptionContains("stationA") );

        try {
            marker.click();
            UiObject desc = device.findObject( new UiSelector().descriptionContains( mActivityRule.getActivity().getResources().getString(R.string.map_click_to_see)));
            desc.click();

            onView(withId(R.id.station_last_hour_container))
                    .check(matches(isDisplayed()));
        } catch (Exception e){}

        onView(isRoot()).perform(pressBack());
        withText(R.string.map)
                .matches(onView(withId(R.id.toolbar)));
    }

    // NAV-006
    @Test
    public void onStationFromMapPressBack_openDrawerAndShowCity(){
        openDrawer(R.id.drawer);

        onView(withText(R.string.map))
                .perform(click());

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        UiObject marker = device.findObject( new UiSelector().descriptionContains("stationA") );

        try {
            marker.click();
            UiObject desc = device.findObject( new UiSelector().descriptionContains( mActivityRule.getActivity().getResources().getString(R.string.map_click_to_see)));
            desc.click();

            onView(withId(R.id.station_last_hour_container))
                    .check(matches(isDisplayed()));
        } catch (Exception e){}

        openDrawer(R.id.drawer);
        onView(withText(R.string.city))
                .perform(click());
        onView(withId(R.id.city_selector_recycler_view))
                .check(matches(isDisplayed()));
    }

    // NAV-007
    @Test
    public void onStationFromCityPressed_showStation(){
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.city_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.station_last_hour_container))
                .check(matches(isDisplayed()));
    }

    // NAV-008
    @Test
    public void onStationView_showHistorical(){
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.city_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.toolbar_station_historical))
                .perform(click());

        onView(withId(R.id.historical_chart))
                .check(matches(isDisplayed()));
    }

    // NAV-009
    @Test
    public void onHistoricalPressBack_showStation(){
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.city_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.toolbar_station_historical))
                .perform(click());

        onView(isRoot()).perform(pressBack());

        onView(withId(R.id.station_last_hour_container))
                .check(matches(isDisplayed()));
    }

    // NAV-010
    @Test
    public void onCityPressSelector_showSelector(){
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.toolbar_city_select))
                .perform(click());

        onView(withId(R.id.city_selector_recycler_view))
                .check(matches(isDisplayed()));
    }
}

