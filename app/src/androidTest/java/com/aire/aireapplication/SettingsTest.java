package com.aire.aireapplication;

import android.content.SharedPreferences;
import android.support.test.filters.LargeTest;

import com.aire.aireapplication.utils.Keys;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static android.content.Context.MODE_PRIVATE;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;

/**
 * Created by Miguel on 28/05/2018.
 */

@LargeTest
public class SettingsTest extends AireInstrumentalTest{

    @Override
    @Before
    public void beforeTest(){
        clearPreferences();

        onView(isRoot()).perform(pressBack());
        openDrawer(R.id.drawer);
    }

    @Override
    @After
    public void afterTest(){
        clearPreferences();
    }

    public void clearPreferences(){
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, true);
        editor.putBoolean(Keys.PREF_SOUND_ENABLED, false);
        editor.putBoolean(Keys.PREF_VIBRATION_ENABLED, false);
        editor.putInt(Keys.PREF_ALERT_LEVEL, 0);
        editor.commit();
    }

    // SET-001(A)
    @Test
    public void notificationsEnabled_disable(){
        // dado que por defecto está activado, las DESACTIVAMOS
        onView(withId(R.id.notifications_switch))
                .perform(click());

        // comprobamos que se DESACTIVAN
        onView(withId(R.id.vibration_switch))
                .check(matches(not(isEnabled())));

        onView(withId(R.id.sound_switch))
                .check(matches(not(isEnabled())));

        onView(withId(R.id.notification_settings_spinner))
                .check(matches(not(isEnabled())));

        // comprobamos las preferencias
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertFalse(sharedPref.getBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, true));
    }

    // SET-001(B)
    @Test
    public void notificationsDisabled_enable(){
        // primero hay que desactivarlas...
        onView(withId(R.id.notifications_switch))
                .perform(click());
        // y las volvemos a activar!
        onView(withId(R.id.notifications_switch))
                .perform(click());

        // comprobamos que se quedan ACTIVOS
        onView(withId(R.id.vibration_switch))
                .check(matches(isEnabled()));

        onView(withId(R.id.sound_switch))
                .check(matches(isEnabled()));

        onView(withId(R.id.notification_settings_spinner))
                .check(matches(isEnabled()));

        // comprobamos las preferencias
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertTrue(sharedPref.getBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, true));
    }

    // SET-002
    @Test
    public void sound_isCorrect(){
        // activamos
        onView(withId(R.id.sound_switch))
                .perform(click());

        // comprobamos las preferencias
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertTrue(sharedPref.getBoolean(Keys.PREF_SOUND_ENABLED, false));

        // desactivamos
        onView(withId(R.id.sound_switch))
                .perform(click());

        // comprobamos las preferencias de nuevo
        sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertFalse(sharedPref.getBoolean(Keys.PREF_SOUND_ENABLED, false));
    }

    // SET-003
    @Test
    public void vibration_isCorrect(){
        // activamos
        onView(withId(R.id.vibration_switch))
                .perform(click());

        // comprobamos las preferencias
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertTrue(sharedPref.getBoolean(Keys.PREF_VIBRATION_ENABLED, false));

        // desactivamos
        onView(withId(R.id.vibration_switch))
                .perform(click());

        // comprobamos las preferencias de nuevo
        sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertFalse(sharedPref.getBoolean(Keys.PREF_VIBRATION_ENABLED, false));
    }

    // SET-004
    @Test
    public void alertLevel_isCorrect(){
        String toxic = App.getContext().getResources().getString(R.string.aiq_hazardous);

        // vamos a hacerle click al toxic
        onView(withId(R.id.notification_settings_spinner))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is(toxic)))
                .perform(click());

        SharedPreferences sharedPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        assertEquals(sharedPref.getInt(Keys.PREF_ALERT_LEVEL, 0), 3);
    }
}
