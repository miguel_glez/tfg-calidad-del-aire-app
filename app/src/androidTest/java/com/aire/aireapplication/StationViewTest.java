package com.aire.aireapplication;

import android.support.test.espresso.contrib.RecyclerViewActions;

import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.PollutionStaticData;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.aire.aireapplication.utils.EspressoTestMatchers.clickChildViewWithId;
import static com.aire.aireapplication.utils.EspressoTestMatchers.withDrawable;

/**
 * Created by Miguel on 28/05/2018.
 */

public class StationViewTest extends AireInstrumentalTest {
    @Before
    public void beforeTest(){
        super.beforeTest();

        // de esta forma todos los tests empezarán con la vista de la estación B
        onView(withId(R.id.city_selector_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.city_selector_adapter_name)));

        onView(withId(R.id.city_recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

    // SV-001
    @Test
    public void basicInfoFragment_isCorrect(){
        // la estación B es la 10001
        Station station = repository.stationDAO.getStationFromIDAsync(10001);

        // sacamos la más contaminada
        String maxPolluter = station.getLatestReading().getDominentPolluter();
        int aqi = station.getLatestReading().getAirQuality().intValue();
        if (maxPolluter.equals("PM25")) maxPolluter = "PM2.5";

        // vamos comprobando que los 4 elementos posibles sean acordes a la información
        // los métodos usados... ya tienen su prueba unitaria correspondiente
        onView(withId(R.id.basic_info_aqi))
                .check(matches(withText(String.valueOf(aqi))));

        onView(withId(R.id.basic_info_max_polluter))
                .check(matches(withText(maxPolluter)));

        onView(withId(R.id.basic_info_description))
                .check(matches(withText(PollutionStaticData.getDescription(aqi))));

        onView(withId(R.id.basic_info_image))
                .check(matches(withDrawable(PollutionStaticData.getPollutionImage(aqi))));
    }

    // SV-002
    @Test
    public void lastReadingDate_isCorrect(){
        // la estación B es la 10001
        Station station = repository.stationDAO.getStationFromIDAsync(10001);

        onView(withId(R.id.last_hour_updated_at))
                .check(matches(withText(PollutionStaticData.getDate(station.getLatestReading().getDate()))));
    }
}
