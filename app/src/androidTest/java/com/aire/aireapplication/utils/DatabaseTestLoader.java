package com.aire.aireapplication.utils;

import android.os.AsyncTask;

import com.aire.aireapplication.persistence.dao.CityDAO;
import com.aire.aireapplication.persistence.dao.ReadingDAO;
import com.aire.aireapplication.persistence.dao.StationDAO;
import com.aire.aireapplication.persistence.database.AireDatabase;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.repository.AireRepository;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Miguel on 28/05/2018.
 */

public class DatabaseTestLoader extends AsyncTask<Void, Void, Void> {
    CityDAO cityDAO;
    StationDAO stationDAO;
    ReadingDAO readingDAO;

    public DatabaseTestLoader(AireRepository repository){
        cityDAO = repository.cityDAO;
        stationDAO = repository.stationDAO;
        readingDAO = repository.readingDAO;
    }

    public DatabaseTestLoader(AireDatabase database){
        cityDAO = database.cityDAO();
        stationDAO = database.stationDAO();
        readingDAO = database.readingDAO();
    }

    @Override
    public Void doInBackground(Void... voids) {

        City cityA = new City(1000, "cityA", false);
        City cityB = new City( 1001, "cityB", true);
        City cityC = new City( 1002, "cityC", false);
        City cityD = new City( 1003, "cityD", true);
        City cityE = new City (1004, "cityE", false);

        cityDAO.insertCity(cityA);
        cityDAO.insertCity(cityB);
        cityDAO.insertCity(cityC);
        cityDAO.insertCity(cityD);
        cityDAO.insertCity(cityE);

        Station sA = new Station(10000, "stationA", 43.34403,-5.97372, 1000);
        Station sB = new Station(10001, "stationB", 43.54898,-5.89722, 1001);
        Station sC = new Station(10002, "stationC", 43.55881, -5.92788, 1002);
        Station sD = new Station(10003, "stationD", 43.55428,-5.91823, 1003);
        Station sE = new Station(10004, "stationE", 43.57532,-5.95849, 1004);
        Station sF = new Station(10005, "stationF", 43.55428,-5.95849, 1001);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -3);
        Date dateBefore3Days = cal.getTime();

        Reading rA = new Reading(sA, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rB = new Reading(sB, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rC = new Reading(sC, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rD = new Reading(sD, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rE = new Reading(sE, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rF = new Reading(sF, dateBefore3Days.getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);

        Reading rLA = new Reading(sA, new Date().getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rLB = new Reading(sB, new Date().getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rLC = new Reading(sC, new Date().getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rLD = new Reading(sD, new Date().getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rLE = new Reading(sE, new Date().getTime(), "PM25", 15.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 15.0, 7.0, 0.0);
        Reading rLF = new Reading(sF, new Date().getTime(), "PM10", 18.0, 0.0, 0.0, 10.0, 5.0, 0.0, 18.0, 15.0, 7.0, 0.0);


        sA.setLatestReading(rLA);
        sB.setLatestReading(rLB);
        sC.setLatestReading(rLC);
        sD.setLatestReading(rLD);
        sE.setLatestReading(rLE);
        sF.setLatestReading(rLF);

        stationDAO.insertStation(sA);
        stationDAO.insertStation(sB);
        stationDAO.insertStation(sC);
        stationDAO.insertStation(sD);
        stationDAO.insertStation(sE);
        stationDAO.insertStation(sF);

        readingDAO.insertReading(rA);
        readingDAO.insertReading(rB);
        readingDAO.insertReading(rC);
        readingDAO.insertReading(rD);
        readingDAO.insertReading(rE);
        readingDAO.insertReading(rF);
        readingDAO.insertReading(rLA);
        readingDAO.insertReading(rLB);
        readingDAO.insertReading(rLC);
        readingDAO.insertReading(rLD);
        readingDAO.insertReading(rLE);
        readingDAO.insertReading(rLF);

        return null;
    }
}
