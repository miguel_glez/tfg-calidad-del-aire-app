package com.aire.aireapplication;

import android.app.Application;
import android.content.Context;

import com.aire.aireapplication.notification.AireJobCreator;
import com.aire.aireapplication.notification.NotificationService;
import com.aire.aireapplication.utils.AppLifecycleHandler;
import com.evernote.android.job.JobManager;

public class App extends Application {

    private static Application sApplication;
    private static boolean jobScheduled = false;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.sApplication = this;
        JobManager.create(this).addJobCreator(new AireJobCreator());
        registerActivityLifecycleCallbacks(new AppLifecycleHandler());

        if (this.jobScheduled == false){
            NotificationService.scheduleJob();
            this.jobScheduled = true;
        }
    }

}