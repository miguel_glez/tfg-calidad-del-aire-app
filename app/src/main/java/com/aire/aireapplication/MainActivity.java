package com.aire.aireapplication;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.view.CityFragment;
import com.aire.aireapplication.view.CitySelectorFragment;
import com.aire.aireapplication.view.CustomMapFragment;
import com.aire.aireapplication.view.InformationFragment;
import com.aire.aireapplication.view.SettingsFragment;
import com.aire.aireapplication.view.StationFragment;
import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ColorPickerDialogFragment.ColorPickerDialogListener {
    private DrawerLayout drawer;
    private NavigationView navView;
    private ActionBarDrawerToggle drawerToggle;
    private Toolbar toolbar;

    boolean mToolBarNavigationListenerIsRegistered;

    boolean showCityInsteadOfMap; // para la navegacion: hay que saber si, en caso de dar dos pasos atras, mostramos ciudad o mapa
    public String showingFragmentTAG;
    public String previousFragmentTAG;
    Fragment fragment;
    String toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // crear actividad con el drawer e instanciar nuestros elementos declarados arriba
        Log.i("MAIN_ACTIVITY", "CREANDO APP...");

        // IMPORTANTISIMO: Poner el theme que toca, porque de mano cargamos la launch screen!
        setTheme(R.style.AppTheme);

        // vamos cargando cosas
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer = findViewById(R.id.drawer);
        navView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        // esa toolbar va a ser el supportActionBar
        setSupportActionBar(toolbar);

        // bindear el drawer a los selectores
        navView.setNavigationItemSelectedListener(this);

        // Boton de abrir y cerrar el drawer
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        // insertar el layout de la ventana principal
        int cityID = getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE).getInt(Keys.PREF_SELECTED_CITY, -1);

        // si tenemos una ciudad guardada, abrimos directamente esa ciudad, sino, que escoja una
        if (savedInstanceState == null)
            if (cityID > 0)
                switchView(R.id.nav_city, cityID);
            else{
                // siento que abro la aplicación por primera vez
                switchView( -1 , -1);
                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.favorite_dialog_title))
                        .setMessage(getResources().getString(R.string.favorite_dialog_text))
                        .setNeutralButton(getResources().getString(R.string.favorite_dialog_button), null)
                        .show();

            }

        else {
            showCityInsteadOfMap = savedInstanceState.getBoolean("showCityInsteadOfMap");
            showingFragmentTAG = savedInstanceState.getString("showingFragment");
            previousFragmentTAG = savedInstanceState.getString("previousFragment");
            setCityName(savedInstanceState.getString("cityName"));
            //switchFragment(fm.getFragment(savedInstanceState, showingFragmentTAG), showingFragmentTAG);
            setTopBarTitle( savedInstanceState.getString("toolbarTitle") );
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // guardar el menu seleccionado
        savedInstanceState.putString("showingFragment", showingFragmentTAG);
        savedInstanceState.putString("previousFragment", previousFragmentTAG);
        savedInstanceState.putString("toolbarTitle", toolbarTitle);
        savedInstanceState.putString("cityName", navView.getMenu().getItem(0).getTitle().toString() );
        savedInstanceState.putBoolean("showCityInsteadOfMap", showCityInsteadOfMap);
        //getSupportFragmentManager().putFragment(savedInstanceState, showingFragmentTAG, fragment);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        switchView(item.getItemId(), -1);
        drawer.closeDrawers();
        return true;
    }

    public void switchView(int itemID, int args){
        if (itemID != 0) {
            Fragment fragment = null;
            switch (itemID) {
                case R.id.nav_city:
                    SharedPreferences sharedPref = getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
                    int cityID = sharedPref.getInt(Keys.PREF_SELECTED_CITY,0);

                    if (cityID > 0 || args > 0) {
                        showingFragmentTAG = Keys.CITY_FRAGMENT_TAG;
                        fragment = CityFragment.newInstance(args > 0? args : cityID);
                    }
                    else
                        fragment = CitySelectorFragment.newInstance(null, false);

                        setToolBarElevation(8);
                    break;

                case R.id.nav_map:
                    setTopBarTitle(getResources().getString(R.string.map));
                    showingFragmentTAG = Keys.MAP_FRAGMENT_TAG;
                    fragment = CustomMapFragment.newInstance();
                    setToolBarElevation(8);
                    break;

                case R.id.nav_info:
                    /*Reading r = new Reading();
                    r.setReadingID(0);
                    r.setAirQuality(120.0);
                    r.setDate(new Date().getTime());
                    r.setDominentPolluter("O3");
                    NotificationLauncher.launch(new City(8, "Mieres", false), r);*/

                    setTopBarTitle(getResources().getString(R.string.info));
                    showingFragmentTAG = Keys.INFO_FRAGMENT_TAG;
                    fragment = InformationFragment.newInstance();
                    setToolBarElevation(0);
                    break;

                case R.id.nav_settings:
                    setTopBarTitle(getResources().getString(R.string.settings));
                    showingFragmentTAG = Keys.SETTINGS_FRAGMENT_TAG;
                    fragment = SettingsFragment.newInstance();
                    setToolBarElevation(8);
                    break;

                default:
                    fragment = CitySelectorFragment.newInstance(null, false);
                    showingFragmentTAG = Keys.CITY_SELECTOR_FRAGMENT_TAG;
                    setToolBarElevation(8);
                    break;
            }

            if (fragment != null) {
                showBackButton(false);
                updateFragmentInfo(showingFragmentTAG);
                switchFragment(fragment, showingFragmentTAG);
            }
        }
    }

    // elevation en dp
    public void setToolBarElevation( float elevation ){
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, elevation, getResources().getDisplayMetrics());

        if (Build.VERSION.SDK_INT >= 21){
            findViewById(R.id.app_bar_layout).setElevation( px );
        }
    }

    /**
     * Sirve para actualizar la informacion en la actividad al cambiar el fragment desde otro, de forma
     * que al guardar el estado de la aplicacion podamos guardar un sub fragmento de otro. (ej histórico de
     * estación)
     * @param tag
     */
    public void updateFragmentInfo( String tag ){
        Log.i("MAIN_ACTIVITY", "UPDATE TO FRAGMENT: "+tag);

        this.previousFragmentTAG = this.showingFragmentTAG;
        this.showingFragmentTAG = tag;

        if (this.previousFragmentTAG.equals(Keys.CITY_FRAGMENT_TAG))
            this.showCityInsteadOfMap = true;
        else if (this.previousFragmentTAG.equals(Keys.MAP_FRAGMENT_TAG))
            this.showCityInsteadOfMap = false;
    }

    public void switchFragment( Fragment fragment, String tag, int selected ){
        Log.i("MAIN_ACTIVITY", "SWITCH TO FRAGMENT: "+tag);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, tag);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();

        if (selected >= 0)
            navView.getMenu().getItem(selected).setChecked(true);
        else if (selected == -2){
            int size = navView.getMenu().size();
            for (int i = 0; i < size; i++) {
                navView.getMenu().getItem(i).setChecked(false);
            }
        }

        if (tag != null) {
            if (tag.equals(Keys.MAP_FRAGMENT_TAG))
                setTopBarTitle(getResources().getString(R.string.map));
        }

        this.fragment = fragment;
    }

    public void switchFragment(Fragment fragment, String tag){
        switchFragment(fragment, tag, -1);
    }

    public void showBackButton(boolean enable) {
         if(enable) {
            drawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if(!mToolBarNavigationListenerIsRegistered) {
                drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            drawerToggle.setDrawerIndicatorEnabled(true);
            drawerToggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }
    }


    @Override
    public void onBackPressed(){
        if (drawer.isDrawerOpen(Gravity.LEFT)){
            this.finish();
            System.exit(0);
        }

        FragmentManager fm = getSupportFragmentManager();
        Fragment fr = fm.findFragmentByTag(previousFragmentTAG);

        boolean showDrawer = showingFragmentTAG.equals(Keys.CITY_FRAGMENT_TAG)
                            || showingFragmentTAG.equals(Keys.MAP_FRAGMENT_TAG)
                            || showingFragmentTAG.equals(Keys.INFO_FRAGMENT_TAG)
                            || showingFragmentTAG.equals(Keys.SETTINGS_FRAGMENT_TAG);

        // CASOS:
        // A) estamos en una estacion y no venimos del mapa: mostramos la ciudad o el mapa (lo ultimo visto)
        if (showingFragmentTAG.equals(Keys.STATION_FRAGMENT_TAG) && (previousFragmentTAG == null || !previousFragmentTAG.equals(Keys.MAP_FRAGMENT_TAG))){
            if (showCityInsteadOfMap)
                setCityFragment();
            else
                switchView(R.id.nav_map, -1);
        }
        // B) Hay un fragmento anterior y no estamos en una de las pantallas accesibles desde el drawer: vamos atrás
        else if (fr != null && previousFragmentTAG != null && !showDrawer) {
            switchFragment(fr, previousFragmentTAG);
            this.showingFragmentTAG = previousFragmentTAG;

            showBackButton(false);
            this.previousFragmentTAG = null;

            int selected = Keys.getNewSelection(showingFragmentTAG);
            if (selected >= 0)
                navView.getMenu().getItem(selected).setChecked(true);
        }
        // C) Estamos en una de las pantallas accesibles desde el drawer: mostramos el drawer
        else{
            drawer.openDrawer(Gravity.LEFT);
        }
    }

    // necesario para que los fragmentos puedan
    // ir cambiando el titulo de la barra segun sea necesario
    public void setTopBarTitle( String title ) {
        getSupportActionBar().setTitle( title );
        toolbarTitle = title;
        Log.i("MAIN ACTIVITY", "toolbar title "+title);
    }

    public void setStationFragment( int stationID ){
        StationFragment stationFragment = StationFragment.newInstance(stationID);
        updateFragmentInfo(Keys.STATION_FRAGMENT_TAG);
        switchFragment(stationFragment, Keys.STATION_FRAGMENT_TAG, -2);
    }

    public void setCityFragment( City city ){
        CityFragment cityFragment = CityFragment.newInstance(city.getId());
        setCityName( city.getName() );

        SharedPreferences notificationPref = getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = notificationPref.edit();
        editor.putInt(Keys.PREF_SELECTED_CITY, city.getId());
        editor.commit();

        updateFragmentInfo(Keys.CITY_FRAGMENT_TAG);
        switchFragment(cityFragment, Keys.CITY_FRAGMENT_TAG, 0);
    }

    public boolean setCityFragment(){
        SharedPreferences sharedPref = getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        int cityID = sharedPref.getInt(Keys.PREF_SELECTED_CITY,0);
        if (cityID > 0) {
            CityFragment cityFragment = CityFragment.newInstance(cityID);
            updateFragmentInfo(Keys.CITY_FRAGMENT_TAG);
            switchFragment(cityFragment, Keys.CITY_FRAGMENT_TAG, 0);

            return true;
        }
        return false;
    }

    public void setCityName( String cityName ){
        navView.getMenu().getItem(0).setTitle( cityName );
    }

    // Debido a que el dialogo llama a la actividad y no al fragmento, hay que llamar al fragmento
    @Override
    public void onColorSelected(int dialogId, int color) {
        if (this.fragment instanceof ColorPickerDialogFragment.ColorPickerDialogListener)
            ((ColorPickerDialogFragment.ColorPickerDialogListener) this.fragment).onColorSelected(dialogId, color);
    }

    @Override
    public void onDialogDismissed(int dialogId) {
        if (this.fragment instanceof ColorPickerDialogFragment.ColorPickerDialogListener)
            ((ColorPickerDialogFragment.ColorPickerDialogListener) this.fragment).onDialogDismissed(dialogId);
    }
}