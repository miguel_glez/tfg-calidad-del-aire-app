package com.aire.aireapplication.network;

import com.aire.aireapplication.utils.Keys;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by Miguel on 20/04/2018.
 */

public class Counter {
    private static Counter instance;
    private HashMap<String, Date> data;
    private boolean toastShown;

    private Counter(){
        data = new HashMap<>();
        toastShown = false;
    }

    public static Counter getInstance(){
        if (instance == null){
            instance = new Counter();
        }
        return instance;
    }

    public boolean doCall(String url){
        Date now = new Date();
        Date disabledUntil = data.get(url);

        if (disabledUntil != null){
           if (disabledUntil.before(now)){
               now.setTime(now.getTime() + Keys.MILLIS_TO_WAIT);
               data.put(url, now);
               return true;
           }else{
               return false;
           }
        }

        now.setTime(now.getTime() + Keys.MILLIS_TO_WAIT);
        data.put(url, now);
        return true;
    }

    public boolean showToast(){
        if (!this.toastShown){
            this.toastShown = true;
            return true;
        }
        return false;
    }
}
