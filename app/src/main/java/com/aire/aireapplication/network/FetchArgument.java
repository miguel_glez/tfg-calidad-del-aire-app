package com.aire.aireapplication.network;

/**
 * Created by Miguel on 02/04/2018.
 */

public class FetchArgument {
    private String argument;
    private String value;

    public FetchArgument(String argument, String value) {
        this.argument = argument;
        this.value = value;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
