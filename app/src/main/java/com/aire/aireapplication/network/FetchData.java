package com.aire.aireapplication.network;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;
import com.aire.aireapplication.utils.JSONUtils;

import org.json.JSONObject;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

/**
 * Created by Miguel on 02/04/2018.
 */

public class FetchData extends AsyncTask<String, Void, JSONObject> {

    private OnDataReceived receiver;
    private String url;
    private String operation;
    private boolean failed;

    public FetchData(OnDataReceived receiver, String operation, ArrayList<FetchArgument> args){
        this.receiver = receiver;
        this.operation = operation;
        this.url = prepareURL(operation, args);
    }

    @Override
    public JSONObject doInBackground(String... args) {
        Counter counter = Counter.getInstance();
        if (counter.doCall(this.url)) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                Log.i("FETCH DATA", this.url);
                String response = restTemplate.getForObject(this.url, String.class, "Android");

                this.failed = false;

                return JSONUtils.getJSONObject(response);
            }catch (Exception exception){
                Log.i("FETCH DATA", "Failed url: "+this.url+"\nError: "+exception.getMessage());
                this.failed = true;
            }

        }else{
            cancel(true);
        }
        return null;
    }

    @Override
    public void onPostExecute(JSONObject json) {
        if (json!=null) {
            receiver.onDataReceived(json, operation);
        }

        if (failed){
            if (Counter.getInstance().showToast()) {
                Toast.makeText(App.getContext(), App.getContext().getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            }
        }
    }

    private static String prepareURL(String operation, ArrayList<FetchArgument> args){
        String url = String.format( "%s%s?", FetchOperations.HOST, operation);

        if (args != null && !args.isEmpty()){
            FetchArgument first = args.get(0);
            url = url.concat(String.format("%s=%s", first.getArgument(), first.getValue()));
            args.remove(0);

            for (FetchArgument entry: args){
                url = url.concat(String.format("&%s=%s", first.getArgument(), first.getValue()));
            }
        }

        return url;
    }

}
