package com.aire.aireapplication.network;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;

/**
 * Created by Miguel on 02/04/2018.
 */

public class FetchOperations {
    private FetchOperations(){};

    public static final String HOST = App.getContext().getResources().getString(R.string.host);
    // operaciones de la api
    public static final String STATION_ALL = "/station/all";
    public static final String STATION_GET = "/station/get";
    public static final String STATION_GET_LAST_DAY = "/station/getLastDay";
    public static final String STATION_GET_LAST_WEEK = "/station/getLastWeek";
    public static final String CITY_STATIONS = "/city/stations";
}
