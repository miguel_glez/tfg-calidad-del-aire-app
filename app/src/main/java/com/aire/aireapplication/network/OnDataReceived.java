package com.aire.aireapplication.network;

import org.json.JSONObject;

/**
 * Created by Miguel on 02/04/2018.
 */

public interface OnDataReceived {
    void onDataReceived( JSONObject json, String operation );
}
