package com.aire.aireapplication.notification;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by Miguel on 11/05/2018.
 */

public class AireJobCreator implements JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case NotificationService.TAG:
                return new NotificationService();
            default:
                return null;
        }
    }
}