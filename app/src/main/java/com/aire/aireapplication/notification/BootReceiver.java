package com.aire.aireapplication.notification;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;

/**
 * Created by Miguel on 21/05/2018.
 */

public class BootReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("ALARM", "ALARM RECEIVED ("+intent.getAction()+")");

        // ejecutar un trabajo
        ComponentName comp = new ComponentName(context.getPackageName(), NotificationService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));

        // Cuando se inicie el dispositivo, programamos un trabajo
        NotificationService.scheduleJob();
    }
}
