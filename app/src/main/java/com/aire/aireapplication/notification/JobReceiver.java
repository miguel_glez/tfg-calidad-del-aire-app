package com.aire.aireapplication.notification;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static android.support.v4.content.WakefulBroadcastReceiver.startWakefulService;

/**
 * Created by Miguel on 11/05/2018.
 */

public class JobReceiver extends BroadcastReceiver{
    // que hacer cuando nos salte la alarma
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("ALARM", "ALARM RECEIVED");

        ComponentName comp = new ComponentName(context.getPackageName(), NotificationService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
    }

}
