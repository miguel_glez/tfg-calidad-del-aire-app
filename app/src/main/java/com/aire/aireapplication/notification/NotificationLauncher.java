package com.aire.aireapplication.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.aire.aireapplication.App;
import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.utils.PollutionStaticData;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Miguel on 11/05/2018.
 */

public class NotificationLauncher {
    private NotificationLauncher(){}

    public static void launch(City city, Reading reading){
        Log.i("NOTIFICATION_LAUNCHER", "LAUNCHING NOTIFICATION");
        SharedPreferences notificationsPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);

        String effects = PollutionStaticData.getEffects( reading.getAirQuality().intValue() );
        String quality = PollutionStaticData.getDescription(reading.getAirQuality().intValue() );

        String airQuality = App.getApplication().getResources().getString(R.string.air_quality);
        String in = App.getApplication().getResources().getString(R.string.in);

        // hacemos la notificación básica
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(App.getContext(), city.getName())
                        .setSmallIcon(R.drawable.ic_station)
                        .setContentTitle(airQuality + " " + in + " " + city.getName())
                        .setContentText(quality);
                        //.setStyle(new NotificationCompat.BigTextStyle().bigText(effects));

        // le añadimos la vibracion
        if (notificationsPref.getBoolean(Keys.PREF_VIBRATION_ENABLED, false)){
            mBuilder.setVibrate(new long[]{1000, 600, 800, 600, 800});
        }

        // le añadimos el sonido
        if (notificationsPref.getBoolean(Keys.PREF_SOUND_ENABLED, false)){
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }

        // le añadimos el led
        mBuilder.setLights(notificationsPref.getInt(Keys.PREF_LED_COLOR, Keys.DEFAULT_LED_COLOR), 750, 2000);

        // le metemos la prioridad
        if (Build.VERSION.SDK_INT >= 16) {
            mBuilder.setPriority(Notification.PRIORITY_MAX);
        }

        Intent intent = new Intent(App.getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(App.getContext(), 0, intent, 0);
        mBuilder.setContentIntent(pendingIntent);
        Log.i("NOTIFICATION_CITY", "id: "+city.getId() );

        notificationsPref.edit().putInt(Keys.PREF_SELECTED_CITY, city.getId()).commit();

        // creamos la notificacion
        Notification notification = mBuilder.build();
        if (Build.VERSION.SDK_INT >= 16){
            int aqi = reading.getAirQuality().intValue();

            // le añadimos un layout con cosas al expandirse
            RemoteViews expandedView = new RemoteViews(App.getContext().getPackageName(), R.layout.notification_expanded);
            expandedView.setTextViewText(R.id.notification_expanded_aqi, String.valueOf(aqi));
            expandedView.setTextViewText(R.id.notification_expanded_description, PollutionStaticData.getDescription(aqi));
            expandedView.setTextViewText(R.id.notification_expanded_max_polluter, reading.getDominentPolluter());
            expandedView.setInt(R.id.notification_expanded_root_layout, "setBackgroundColor", PollutionStaticData.getColor(aqi));
            expandedView.setImageViewResource(R.id.notification_expanded_image, PollutionStaticData.getPollutionImage(aqi));

            notification.bigContentView = expandedView;

            // y otro cuando este plegadito
            /*RemoteViews contractedView = new RemoteViews(App.getContext().getPackageName(), R.layout.notification_contracted);
            contractedView.setTextViewText(R.id.notification_contracted_aqi, String.valueOf(aqi));
            contractedView.setInt(R.id.notification_contracted_root_layout, "setBackgroundColor", PollutionStaticData.getColor(aqi));

            notification.contentView = contractedView;*/
            // casi que pasamos
        }

        if (notificationsPref.getBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, true)) {
            if (reading.getAirQuality() > PollutionStaticData.getValueForIndex( notificationsPref.getInt(Keys.PREF_ALERT_LEVEL, 0)+2 )) {
                NotificationManager mNotificationManager = (NotificationManager) App.getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(city.getId() + reading.getReadingID(), notification);
            }
        }
    }
}
