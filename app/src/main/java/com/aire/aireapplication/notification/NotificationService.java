package com.aire.aireapplication.notification;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.aire.aireapplication.App;
import com.aire.aireapplication.network.FetchArgument;
import com.aire.aireapplication.network.FetchData;
import com.aire.aireapplication.network.FetchOperations;
import com.aire.aireapplication.network.OnDataReceived;
import com.aire.aireapplication.persistence.database.FetchCityFromID;
import com.aire.aireapplication.persistence.database.OnCityReceived;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.repository.AireRepository;
import com.aire.aireapplication.utils.AppLifecycleHandler;
import com.aire.aireapplication.utils.JSONUtils;
import com.aire.aireapplication.utils.Keys;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Miguel on 11/05/2018.
 */

public class NotificationService extends Job implements OnDataReceived, OnCityReceived {
    public static final String TAG = "NOTIFICATION_SERVICE";

    private AireRepository repository;
    private HashMap<Integer, ArrayList<Station>> stations;

    public static void scheduleJob() {
        Log.i(TAG, "SCHEDULING JOB.");

        int jobID = new JobRequest.Builder(NotificationService.TAG)
                .setPeriodic(Keys.MILLIS_TO_FETCH_DATA ,5* 60 * 1000)
                //.setPeriodic(15* 60 * 1000, 14*60*1000)
                .build()
                .schedule();

        for (int i=0; i<jobID-1; i++){
            JobManager.instance().cancel(i);
        }
    }

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        this.repository = new AireRepository(App.getApplication());
        this.stations = new HashMap<>();

        for (Station station: repository.getFavoriteCityStations()){
            ArrayList<FetchArgument> args = new ArrayList<>();
            args.add( new FetchArgument("id", String.valueOf(station.getId()) ) );
            FetchData task = new FetchData(this, FetchOperations.STATION_GET, args );
            JSONObject object = task.doInBackground();
            if (object != null)
                onDataReceived(object, FetchOperations.STATION_GET);
            else
                return Result.FAILURE;
        }

        for (Integer cityID: stations.keySet()) {
            getCityName(cityID);
        }
        return Result.SUCCESS;
    }

    @Override
    public void onDataReceived(JSONObject json, String operation) {
        // actualizamos los datos en bd
        String stationJSON = json.optString("station");
        Station station = JSONUtils.deserializeStation(stationJSON);

        String latestReadingJSON = json.optString("latestReading");
        Reading latestReading = JSONUtils.deserializeReading(latestReadingJSON);
        station.setLatestReading(latestReading);
        new AireRepository.insertStationAsyncTask(this.repository.stationDAO).execute(station);

        ArrayList<Station> list;
        if (this.stations.get(station.getCityID()) != null)
            list = this.stations.get(station.getCityID());
        else
            list = new ArrayList<>();

        list.add(station);
        this.stations.put(station.getCityID(), list);
    }

    private void getCityName(int cityID){
        // necesitamos el nombre de la ciudad para la notificacion (solo queremos llamar una vez por ciudad)
        new FetchCityFromID(App.getApplication(), this).execute(cityID);
    }

    @Override
    public void onCityReceived(City city) {
        SharedPreferences notificationsPref = App.getContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        List<Station> cityStations = this.stations.get(city.getId());

        // comprobamos si mandamos notificacion
        for (Station station : cityStations ) {
            Reading latestReading = station.getLatestReading();
            if (NotificationsSent.canBeSent(latestReading.getDateLong(), station.getCityID())) {
                if (!AppLifecycleHandler.isApplicationVisible()) {
                    NotificationLauncher.launch(city, latestReading);

                    // cuando mandamos una notificacion de una ciudad hay que prohibir el resto
                    for ( Station s : cityStations ) {
                        NotificationsSent.canBeSent(s.getLatestReading().getDateLong(), s.getCityID());
                    }
                }
            }
        }

    }
}
