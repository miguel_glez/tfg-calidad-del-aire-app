package com.aire.aireapplication.notification;

import android.content.Context;
import android.util.Log;

import com.aire.aireapplication.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.TreeSet;

/**
 * Created by Miguel on 11/05/2018.
 * Clase pensada únicamente para ver si ya hemos mandado
 * una notificación para una lectura recibida dada.
 */

public class NotificationsSent {
    private static final String TAG = "NOTIFICATIONS_SENT";
    private static HashMap<Integer, TreeSet<Long>> notificationsSent;

    private NotificationsSent() {}

    private static HashMap<Integer, TreeSet<Long>> getInstance() {
        if(notificationsSent == null) {
            notificationsSent = loadMap();
        }
        return notificationsSent;
    }

    public static boolean canBeSent(Long readingTime, Integer cityID){
        HashMap<Integer, TreeSet<Long>> map = getInstance();

        Log.i(TAG, "map "+(map));
        TreeSet<Long> savedDates = map.get(cityID);
        boolean canBeSent = true;

        if (savedDates != null) {
            for (Long rID : savedDates) {
                // CASO DE QUE SE ACTUALICE UN DATO MAS ANTIGUO DE ALGO QUE TENEMOS
                // O YA LO TENEMOS
                if (rID >= readingTime) canBeSent = false;
            }
        } else savedDates = new TreeSet<>();

        if (!savedDates.contains(readingTime)) {
            savedDates.add(readingTime);
            map.put(cityID, savedDates);
            saveMap(map);
        }

        return canBeSent;
    }

    private static HashMap<Integer, TreeSet<Long>> loadMap(){
        try {
            File file = new File(App.getContext().getDir("data", Context.MODE_PRIVATE), "notificationsSent");
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
            HashMap<Integer, TreeSet<Long>> map = (HashMap) inputStream.readObject();
            Log.i(TAG, "LOADING MAP: "+map );

            inputStream.close();

            for (Integer cityID : map.keySet()){
                // no queremos tener un map enorme, si hay que repetir notificacion
                if (map.get(cityID).size() > 100) return new HashMap<>();
            }

            return map;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return new HashMap<>();
    }

    private static void saveMap(HashMap<Integer, TreeSet<Long>> map){
        try {
            File file = new File(App.getContext().getDir("data", Context.MODE_PRIVATE), "notificationsSent");
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));

            outputStream.writeObject(map);
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
