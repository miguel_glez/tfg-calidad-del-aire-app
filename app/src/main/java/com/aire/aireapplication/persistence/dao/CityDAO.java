package com.aire.aireapplication.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aire.aireapplication.persistence.domain.City;

import java.util.List;

/**
 * Created by Miguel on 18/04/2018.
 */

@Dao
public interface CityDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertCity(City city);

    @Update
    void updateCity(City city);

    @Query("SELECT * FROM cities")
    LiveData<List<City>> getAllCities();

    @Query("SELECT * FROM cities WHERE id = :cityID")
    LiveData<City> getCityFromID( int cityID );

    @Query("SELECT * FROM cities WHERE id = :cityID")
    City getCityAsync( int cityID );
}
