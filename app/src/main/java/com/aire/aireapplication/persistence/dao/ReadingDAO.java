package com.aire.aireapplication.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.aire.aireapplication.persistence.domain.Reading;

import java.util.List;

/**
 * Created by Miguel on 18/04/2018.
 */

@Dao
public interface ReadingDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertReading( Reading reading );

    @Query( "SELECT * FROM readings WHERE stationID = :stationID AND datetime(timestamp/1000,'unixepoch','localtime') >= datetime('now', '-24 hour') ORDER BY readingID DESC")
    LiveData<List<Reading>> getStationLastDayReadings(int stationID);

    @Query( "SELECT * FROM readings WHERE stationID = :stationID AND datetime(timestamp/1000,'unixepoch','localtime') >= datetime('now', '-7 day') ORDER BY readingID DESC")
    LiveData<List<Reading>> getStationLastWeekReadings(int stationID);

    @Query( "DELETE FROM readings WHERE datetime(timestamp/1000,'unixepoch','localtime') <= datetime('now', '-7 day')")
    void cleanOldReadings();
}
