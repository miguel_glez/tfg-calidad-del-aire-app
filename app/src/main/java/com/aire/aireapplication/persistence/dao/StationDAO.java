package com.aire.aireapplication.persistence.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.aire.aireapplication.persistence.domain.Station;

import java.util.List;

/**
 * Created by Miguel on 18/04/2018.
 */

@Dao
public interface StationDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStation( Station station );

    @Query("SELECT * FROM stations")
    LiveData<List<Station>> getAllStations();

    @Query("SELECT * FROM stations WHERE id = :stationID")
    LiveData<Station> getStationFromID(int stationID);

    @Query("SELECT * FROM stations WHERE id = :stationID")
    Station getStationFromIDAsync(int stationID);

    @Query("SELECT * FROM stations WHERE cityID= :cityID")
    LiveData<List<Station>> getCityStations(int cityID);

    @Query("SELECT * FROM stations WHERE cityID= :cityID")
    List<Station> getCityStationsAsync(int cityID);

    @Query("SELECT s.* FROM  cities c, stations s WHERE c.id = s.cityID AND c.favorite=1")
    List<Station> getFavoriteCityStations();
}
