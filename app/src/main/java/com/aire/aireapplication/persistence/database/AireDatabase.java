package com.aire.aireapplication.persistence.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.aire.aireapplication.network.FetchData;
import com.aire.aireapplication.network.FetchOperations;
import com.aire.aireapplication.network.OnDataReceived;
import com.aire.aireapplication.persistence.dao.CityDAO;
import com.aire.aireapplication.persistence.dao.ReadingDAO;
import com.aire.aireapplication.persistence.dao.StationDAO;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Miguel on 18/04/2018.
 */

@Database(entities={City.class, Station.class, Reading.class}, version=1)
@TypeConverters({ReadingConverter.class, StationConverter.class, DateConverter.class})
public abstract class AireDatabase extends RoomDatabase {
    public abstract CityDAO cityDAO();
    public abstract StationDAO stationDAO();
    public abstract ReadingDAO readingDAO();

    private static AireDatabase INSTANCE;

    public static AireDatabase getDatabase( final Context context ){
        if (INSTANCE == null){
            synchronized (AireDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AireDatabase.class, "aire_database")
                            .addCallback(sRoomDatabaseCallbackCreate)
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallbackCreate = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new populateDBAsync(INSTANCE).execute();
        }
    };

    private static class populateDBAsync extends AsyncTask<Void, Void, Void>{
        private final CityDAO cDao;
        private final ReadingDAO rDao;

        populateDBAsync(AireDatabase db){
            this.cDao = db.cityDAO();
            this.rDao = db.readingDAO();
        }

        @Override
        protected Void doInBackground(Void... voids){

            // las ciudades no van a cambiar asi que las podemos tener insertadas de mano
            String[] cities = {"Trubia","Gijón","Cangas del Narcea","Grado","Avilés","Oviedo","Lugones","Mieres","San Martín del Rey Aurelio","Río Santa Bárbara","Llaranes", "Salinas","Sama","Langreo", "La Felguera"};
            for (int i=0; i<cities.length; i++) {
                cDao.insertCity(new City(i+1, cities[i], false));
            }

            // limpiamos datos que ya no sirven para nada
            rDao.cleanOldReadings();
            return null;
        }
    }
}
