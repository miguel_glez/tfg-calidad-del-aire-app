package com.aire.aireapplication.persistence.database;

import android.arch.persistence.room.TypeConverter;

import java.sql.Date;

/**
 * Created by Miguel on 18/04/2018.
 */

public class DateConverter {
    @TypeConverter
    public static Date toDate(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long toLong(Date value) {
        return value == null ? null : value.getTime();
    }
}
