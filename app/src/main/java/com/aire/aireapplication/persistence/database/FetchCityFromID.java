package com.aire.aireapplication.persistence.database;

import android.app.Application;
import android.os.AsyncTask;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.repository.AireRepository;

/**
 * Created by Miguel on 11/05/2018.
 */

public class FetchCityFromID extends AsyncTask<Integer, Void, City> {
    AireRepository repository;
    OnCityReceived caller;

    public FetchCityFromID(Application app, OnCityReceived caller){
        this.repository = new AireRepository(app);
        this.caller = caller;
    }

    @Override
    protected City doInBackground(Integer... ints) {
        return repository.getCityAsync(ints[0]);
    }

    @Override
    protected void onPostExecute(City o) {
        caller.onCityReceived(o);
    }
}
