package com.aire.aireapplication.persistence.database;

import com.aire.aireapplication.persistence.domain.City;

/**
 * Created by Miguel on 11/05/2018.
 */

public interface OnCityReceived {
    void onCityReceived( City city );
}
