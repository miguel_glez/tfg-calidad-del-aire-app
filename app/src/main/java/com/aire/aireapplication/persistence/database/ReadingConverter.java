package com.aire.aireapplication.persistence.database;

import android.arch.persistence.room.TypeConverter;

import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.utils.JSONUtils;

/**
 * Created by Miguel on 18/04/2018.
 */

public class ReadingConverter {
    @TypeConverter
    public static Reading toReading(String reading){
        return JSONUtils.deserializeReading(reading);
    }

    @TypeConverter
    public static String fromReading(Reading reading){
        return JSONUtils.serializeReading(reading);
    }
}
