package com.aire.aireapplication.persistence.database;

import android.arch.persistence.room.TypeConverter;

import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.JSONUtils;

/**
 * Created by Miguel on 18/04/2018.
 */

public class StationConverter {
    @TypeConverter
    public static Station toStation(String station){
        return JSONUtils.deserializeStation(station);
    }

    @TypeConverter
    public static String fromStation(Station station){
        return JSONUtils.serializeStation(station);
    }
}
