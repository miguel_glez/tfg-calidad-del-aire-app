package com.aire.aireapplication.persistence.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity(tableName="readings")
public class Reading implements Comparable<Reading>{

    @Expose
    @PrimaryKey
    @NonNull
	private int readingID;

    @Ignore
	private Station station;

    private int stationID;

    @Ignore
    @Expose
    private Long date;

    private Date timestamp;
	
	@Expose
	private String dominentPolluter;	// dominentpol
	
	@Expose
	private Double airQuality;			// aqi
	
	@Expose
	private Double carbonMonoxide; 		// co
	
	@Expose
	private Double humidity;			// h
	
	@Expose
	private Double nitrogenDioxide;		// no2
	
	@Expose
	private Double ozone; 	 			// o3
	
	@Expose
	private Double pressure; 			// p
	
	@Expose
	private Double pm10;				// pm10
	
	@Expose
	private Double pm25;				// pm25
	
	@Expose
	private Double sulfurDioxide;		// so2
	
	@Expose
	private Double temperature; 		// t

    @Ignore
	public Reading(Station station, Long date, String dominentPolluter, Double airQuality, Double carbonMonoxide,
			Double humidity, Double nitrogenDioxide, Double ozone, Double pressure, Double pm10, Double pm25, Double sulfurDioxide,
			Double temperature) {
		this.station = station;
		this.date = date;
		this.dominentPolluter = dominentPolluter;
		this.airQuality = airQuality;
		this.carbonMonoxide = carbonMonoxide;
		this.humidity = humidity;
		this.nitrogenDioxide = nitrogenDioxide;
		this.ozone = ozone;
		this.pressure = pressure;
		this.pm10 = pm10;
		this.pm25 = pm25;
		this.sulfurDioxide = sulfurDioxide;
		this.temperature = temperature;
	}

	public Reading() {
	}

	public String getDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.getTime());

        //.setTimeZone(TimeZone.getTimeZone("UTC"));calendar.getTimeZone();

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format1.format(calendar.getTime());
	}

	public Long getDateLong(){
        return date;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setDate(Long date) {
		this.date = date;
	}

	public String getDominentPolluter() {
		if (dominentPolluter.equals("PM25"))
				return "PM2.5";
		return dominentPolluter;
	}

	public void setDominentPolluter(String dominentPolluter) {
		this.dominentPolluter = dominentPolluter;
	}

	public Double getAirQuality() {
		return airQuality;
	}

	public void setAirQuality(Double airQuality) {
		this.airQuality = airQuality;
	}

	public Double getCarbonMonoxide() {
		return carbonMonoxide;
	}

	public void setCarbonMonoxide(Double carbonMonoxide) {
		this.carbonMonoxide = carbonMonoxide;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public Double getNitrogenDioxide() {
		return nitrogenDioxide;
	}

	public void setNitrogenDioxide(Double nitrogenDioxide) {
		this.nitrogenDioxide = nitrogenDioxide;
	}

	public Double getOzone() {
		return ozone;
	}

	public void setOzone(Double ozone) {
		this.ozone = ozone;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public Double getPm10() {
		return pm10;
	}

	public void setPm10(Double pm10) {
		this.pm10 = pm10;
	}

	public Double getPm25() {
		return pm25;
	}

	public void setPm25(Double pm25) {
		this.pm25 = pm25;
	}

	public Double getSulfurDioxide() {
		return sulfurDioxide;
	}

	public void setSulfurDioxide(Double sulfurDioxide) {
		this.sulfurDioxide = sulfurDioxide;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

    @NonNull
    public int getReadingID() {
        return readingID;
    }

    public void setReadingID(@NonNull int readingID) {
        this.readingID = readingID;
    }

    public int getStationID() {
        return stationID;
    }

    public void setStationID(int stationID) {
        this.stationID = stationID;
    }

    public Double getValue(String value ){
        switch( value.toLowerCase() ) {
            case "pm2.5":
                return getPm25();
            case "pm10":
                return getPm10();
            case "o3":
                return getOzone();
            case "so2":
                return getSulfurDioxide();
            case "aqi":
                return getAirQuality();
            case "co":
                return getCarbonMonoxide();
            case "no2":
                return getNitrogenDioxide();
            default:
                return null;
        }
    }

    public String toString(){
        return getDate();
    }

    @Override
    public int compareTo(@NonNull Reading o) {
        String fecha1 = getDate().toString().split(" ")[0];
        String hora1s = getDate().toString().split( " ")[1];
        int hora1 = Integer.parseInt(hora1s.split(":")[0]);
        int dia1 = Integer.parseInt(fecha1.split("-")[2]);
        int mes1 = Integer.parseInt( fecha1.split( "-")[1]);

        String fecha2 = o.getDate().toString().split(" ")[0];
        String hora2s = o.getDate().toString().split( " ")[1];
        int hora2 = Integer.parseInt(hora2s.split(":")[0]);
        int dia2 = Integer.parseInt(fecha2.split("-")[2]);
        int mes2 = Integer.parseInt( fecha2.split( "-")[1]);


        if (hora1 == hora2 && dia1 == dia2 && mes1 == mes2)
            return 0;

        if (mes1 != mes2){
            if (mes1 > mes2) return 1;
            else return -1;
        }

        if (dia1 != dia2){
            if (dia1 > dia2) return 1;
            else return -1;
        }

        if (hora1 > hora2)
            return 1;
        return -1;
    }
}
