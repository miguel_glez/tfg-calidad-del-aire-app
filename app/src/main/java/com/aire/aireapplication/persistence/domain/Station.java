package com.aire.aireapplication.persistence.domain;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "stations")
public class Station {

    @PrimaryKey
    @NonNull
	private int id;
	private String name;
	
	private Double latitude;
	private Double longitude;

	private int cityID;
	private Reading latestReading;
	

	@Ignore
	public Station(int id, String name, Double latitude, Double longitude) {
		super();
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Ignore
	public Station(int id, String name, Double latitude, Double longitude, int cityID) {
		this(id, name, latitude, longitude);
		this.cityID = cityID;
	}

	@Ignore
	public Station(int id, String name, Double latitude, Double longitude, int cityID, Reading latestReading) {
		this(id, name, latitude, longitude, cityID);
		this.latestReading = latestReading;
	}

	public Station() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public Reading getLatestReading() {
        return latestReading;
    }

    public void setLatestReading(Reading latestReading) {
        this.latestReading = latestReading;
    }

    public int getStationID() {
		return id;
	}
	public void setId(int id){ this.id = id; }
    public int getId() {return id;}
}
