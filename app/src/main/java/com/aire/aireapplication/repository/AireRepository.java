package com.aire.aireapplication.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.aire.aireapplication.network.FetchArgument;
import com.aire.aireapplication.network.FetchData;
import com.aire.aireapplication.network.FetchOperations;
import com.aire.aireapplication.network.OnDataReceived;
import com.aire.aireapplication.persistence.dao.CityDAO;
import com.aire.aireapplication.persistence.dao.ReadingDAO;
import com.aire.aireapplication.persistence.dao.StationDAO;
import com.aire.aireapplication.persistence.database.AireDatabase;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miguel on 18/04/2018.
 */

public class AireRepository implements OnDataReceived{
    AireDatabase db;
    public CityDAO cityDAO;
    public StationDAO stationDAO;
    public ReadingDAO readingDAO;

    public AireRepository(AireDatabase database){
        this.db = database;

        this.cityDAO = this.db.cityDAO();
        this.stationDAO = this.db.stationDAO();
        this.readingDAO = this.db.readingDAO();
    }

    public AireRepository(Application application){
        this(AireDatabase.getDatabase(application));
    }

    public void clearDatabase(){
        new clearDBAsyncTask(this.db).execute();
    }

    public LiveData<City> getCity(int cityID){
        // mandamos a actualizar los datos
        ArrayList<FetchArgument> args = new ArrayList<FetchArgument>();
        args.add( new FetchArgument("id", String.valueOf(cityID) ) );
        FetchData task = new FetchData(this, FetchOperations.CITY_STATIONS, args );
        task.execute();

        return cityDAO.getCityFromID(cityID);
    }

    public City getCityAsync( int cityID ){
        return cityDAO.getCityAsync(cityID);
    }

    public LiveData<List<Station>> getCityStations(int cityID){
        return stationDAO.getCityStations(cityID);
    }

    public LiveData<List<Station>> getAllStations(){
        FetchData task = new FetchData(this, FetchOperations.STATION_ALL, null );
        task.execute();

        return stationDAO.getAllStations();
    }

    public LiveData<Station> getStation( int stationID ){
        ArrayList<FetchArgument> args = new ArrayList<FetchArgument>();
        args.add( new FetchArgument("id", String.valueOf(stationID) ) );
        FetchData task = new FetchData(this, FetchOperations.STATION_GET, args );
        task.execute();

        return stationDAO.getStationFromID(stationID);
    }

    public LiveData<List<Reading>> getLastDayReadings(int stationID){
        ArrayList<FetchArgument> args = new ArrayList<>();
        args.add( new FetchArgument("id", String.valueOf(stationID) ) );
        FetchData task = new FetchData(this, FetchOperations.STATION_GET_LAST_DAY, args );
        task.execute();

        return readingDAO.getStationLastDayReadings(stationID);
    }

    public LiveData<List<Reading>> getLastWeekReadings(int stationID){
        ArrayList<FetchArgument> args = new ArrayList<>();
        args.add( new FetchArgument("id", String.valueOf(stationID) ) );
        FetchData task = new FetchData(this, FetchOperations.STATION_GET_LAST_WEEK, args );
        task.execute();

        return readingDAO.getStationLastWeekReadings(stationID);
    }

    public LiveData<List<City>> getAllCities(){
        return cityDAO.getAllCities();
    }

    public void updateCity(City city){
        new updateCityAsyncTask(cityDAO).execute(city);
    }

    public List<Station> getFavoriteCityStations(){
        return stationDAO.getFavoriteCityStations();
    }

    /*
     A PARTIR DE AQUI GUARDAMOS LOS DATOS BUSCADOS
     */
    @Override
    public void onDataReceived(JSONObject json, String operation) {
        JSONArray readingsJSON;
        ArrayList<Station> list;
        switch(operation){
            case FetchOperations.CITY_STATIONS:
                readingsJSON = json.optJSONArray("stations");
                City city = JSONUtils.deserializeCity(json.toString());

                list = new ArrayList<>();
                for (int i=0; i<readingsJSON.length(); i++){
                    list.add( JSONUtils.deserializeStation(readingsJSON.optString(i), city.getId()) );
                }
                new insertStationAsyncTask(stationDAO).execute(list.toArray(new Station[list.size()]));
                new insertCityAsyncTask(cityDAO).execute(city);
                break;

            case FetchOperations.STATION_ALL:
                readingsJSON = json.optJSONArray("stations");
                list = new ArrayList<>();
                for (int i=0; i<readingsJSON.length(); i++){
                    list.add( JSONUtils.deserializeStation(readingsJSON.optString(i)) );
                }
                new insertStationAsyncTask(stationDAO).execute(list.toArray(new Station[list.size()]));
                break;

            case FetchOperations.STATION_GET:
                String stationJSON = json.optString("station");
                Station station = JSONUtils.deserializeStation(stationJSON);

                String latestReadingJSON = json.optString("latestReading");
                Reading latestReading = JSONUtils.deserializeReading(latestReadingJSON);
                station.setLatestReading(latestReading);

                new insertStationAsyncTask(stationDAO).execute(station);
                break;

            case FetchOperations.STATION_GET_LAST_DAY:
            case FetchOperations.STATION_GET_LAST_WEEK:
                readingsJSON = json.optJSONArray("data");
                ArrayList<Reading> rlist = new ArrayList<>();
                for (int i=0; i<readingsJSON.length(); i++){
                    Reading reading = JSONUtils.deserializeReading(readingsJSON.optString(i));
                    reading.setStationID(json.optInt("stationID"));
                    rlist.add( reading );
                }
                new insertReadingAsyncTask(readingDAO).execute(rlist.toArray(new Reading[rlist.size()]));
                break;

        }

    }

    private static class updateCityAsyncTask extends AsyncTask<City, Void, Void> {
        private CityDAO myAsyncTaskDAO;
        public updateCityAsyncTask(CityDAO myAsyncTaskDAO) {
            this.myAsyncTaskDAO = myAsyncTaskDAO;
        }

        @Override
        protected Void doInBackground(City... cities) {
            myAsyncTaskDAO.updateCity(cities[0]);
            return null;
        }
    }

    private static class insertCityAsyncTask extends AsyncTask<City, Void, Void> {
        private CityDAO myAsyncTaskDAO;
        public insertCityAsyncTask(CityDAO myAsyncTaskDAO) {
            this.myAsyncTaskDAO = myAsyncTaskDAO;
        }

        @Override
        protected Void doInBackground(City... cities) {
            myAsyncTaskDAO.insertCity(cities[0]);
            return null;
        }
    }

    public static class insertStationAsyncTask extends AsyncTask<Station, Void, Void> {
        private StationDAO myAsyncTaskDAO;
        public insertStationAsyncTask(StationDAO myAsyncTaskDAO) { this.myAsyncTaskDAO = myAsyncTaskDAO; }

        @Override
        protected Void doInBackground(Station... stations) {
            for (int i=0; i<stations.length; i++) {
                myAsyncTaskDAO.insertStation(stations[i]);
            }
            return null;
        }
    }

    private static class insertReadingAsyncTask extends AsyncTask<Reading, Void, Void> {
        private ReadingDAO myAsyncTaskDAO;
        public insertReadingAsyncTask(ReadingDAO myAsyncTaskDAO) { this.myAsyncTaskDAO = myAsyncTaskDAO; }

        @Override
        protected Void doInBackground(Reading... readings) {
            for (int i=0; i<readings.length; i++) {
                myAsyncTaskDAO.insertReading(readings[i]);
            }
            return null;
        }
    }

    private static class clearDBAsyncTask extends AsyncTask<Void, Void, Void> {
        private AireDatabase db;
        public clearDBAsyncTask(AireDatabase db) { this.db = db; }

        @Override
        protected Void doInBackground(Void... voids) {
            db.clearAllTables();
            return null;
        }
    }
}
