package com.aire.aireapplication.utils;

import com.aire.aireapplication.persistence.domain.City;

import java.util.Comparator;

/**
 * Created by Miguel on 09/05/2018.
 */

public class CityComparator {
    private CityComparator(){}

    private static final Comparator<City> COMPARATOR = new Comparator<City>() {
        @Override
        public int compare(City a, City b) {
            if (a.isFavorite() == b.isFavorite())
                return a.getName().compareTo(b.getName());
            else {
                if (a.isFavorite()) return -1;
                else return 1;
            }
        }
    };

    public static Comparator<City> getComparator(){
        return COMPARATOR;
    }
}
