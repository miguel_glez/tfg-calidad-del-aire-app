package com.aire.aireapplication.utils;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;

/**
 * Created by Miguel on 02/04/2018.
 */

public class JSONUtils {
    private JSONUtils(){}

    public static String serializeStation( Station station ){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(station);
    }

    public static Station deserializeStation(String station){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.fromJson(station, Station.class);
    }

    public static Station deserializeStation(String stationStr, int cityID){
        Station station = deserializeStation(stationStr);
        station.setCityID(cityID);
        return station;
    }

    public static String serializeCity(City city){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(city);
    }

    public static City deserializeCity(String city ){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.fromJson(city, City.class);
    }

    public static String serializeReading( Reading reading ){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();

        return gson.toJson(reading);
    }

    public static Reading deserializeReading(String reading){
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();

        Reading readingObj = gson.fromJson(reading, Reading.class);
        readingObj.setTimestamp( new Date( readingObj.getDateLong() ) );

        return readingObj;
    }

    public static JSONObject getJSONObject(String string){
        try{
            return new JSONObject(string);
        }
        catch( JSONException e ){ e.printStackTrace(); }
        return null;
    }
}
