package com.aire.aireapplication.utils;

import android.graphics.Color;

/**
 * Created by Miguel on 07/04/2018.
 */

public class Keys {
    private Keys (){}

    public static String STATION_FRAGMENT_TAG = "STATION_FRAGMENT";
    public static String HISTORICAL_FRAGMENT_TAG = "HISTORICAL_FRAGMENT";
    public static String MAP_FRAGMENT_TAG = "MAP_FRAGMENT";
    public static String CITY_FRAGMENT_TAG = "CITY_FRAGMENT";
    public static String SETTINGS_FRAGMENT_TAG = "SETTINGS_FRAGMENT";
    public static String INFO_FRAGMENT_TAG = "INFO_FRAGMENT";
    public static String CITY_SELECTOR_FRAGMENT_TAG = "CITY_SELECTOR_FRAGMENT";
    public static String BASIC_INFO_FRAGMENT_TAG = "BASIC_INFO_FRAGMENT_TAG";
    public static String LAST_HOUR_FRAGMENT_TAG = "LAST_HOUR_FRAGMENT_TAG";

    public static String PREF_SELECTED_CITY = "SELECTED_CITY";
    public static String PREF_NOTIFICATION_FILE = "NOTIFICATIONS_SETTINGS";
    public static String PREF_NOTIFICATIONS_ENABLED = "NOTIFICATIONS_ENABLED";
    public static String PREF_VIBRATION_ENABLED = "VIBRATION";
    public static String PREF_LED_COLOR = "LED_COLOR";
    public static String PREF_SOUND_ENABLED = "SOUND_ENABLED";
    public static String PREF_ALERT_LEVEL = "ALERT_LEVEL";


    public static int DEFAULT_LED_COLOR = Color.CYAN;

    public static int MILLIS_TO_FETCH_DATA = 30 * 60 * 1000;

    public static int getNewSelection( String tag ){
        String[] tags = {CITY_FRAGMENT_TAG, MAP_FRAGMENT_TAG, SETTINGS_FRAGMENT_TAG, INFO_FRAGMENT_TAG};
        for (int i=0;i<tags.length;i++){
            if (tag.equals(tags[i])) return i;
        }
        return -1;
    }

    public static long MILLIS_TO_WAIT = new Long(5*60*1000); // 5 MINUTOS
}
