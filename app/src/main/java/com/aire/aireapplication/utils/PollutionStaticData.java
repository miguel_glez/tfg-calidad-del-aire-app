package com.aire.aireapplication.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

/**
 * Created by Miguel on 02/04/2018.
 */

public class PollutionStaticData {

    private PollutionStaticData(){}

    public static int getColor(int aqi){
        Context context = App.getContext();

        int[] colors = new int[]{ContextCompat.getColor(context, R.color.aiq_good_background),
                ContextCompat.getColor(context, R.color.aiq_moderate_background),
                ContextCompat.getColor(context, R.color.aiq_unhealthy_sensitive_background),
                ContextCompat.getColor(context, R.color.aiq_unhealthy_background),
                ContextCompat.getColor(context, R.color.aiq_very_unhealthy_background),
                ContextCompat.getColor(context, R.color.aiq_hazardous_background)};

        return colors[getIndexForValue(aqi)];
    }

    public static float getMarkerColor(int aqi){
        float[] colors = new float[]{BitmapDescriptorFactory.HUE_GREEN,
                BitmapDescriptorFactory.HUE_YELLOW,
                BitmapDescriptorFactory.HUE_ORANGE,
                BitmapDescriptorFactory.HUE_VIOLET,
                BitmapDescriptorFactory.HUE_MAGENTA,
                BitmapDescriptorFactory.HUE_RED
        };

        return colors[getIndexForValue(aqi)];
    }

    public static int getMarker(int aqi){
        int[] images = { R.drawable.ic_pin_good,
                R.drawable.ic_pin_moderate,
                R.drawable.ic_pin_unhealthy_sensitive,
                R.drawable.ic_pin_unhealthy,
                R.drawable.ic_pin_very_unhealthy,
                R.drawable.ic_pin_toxic
        };

        return images[getIndexForValue(aqi)];
    }

    public static String getDescription(int aqi){
        Resources resources = App.getContext().getResources();
        String[] descriptions = new String[]{resources.getString(R.string.aiq_good),
                resources.getString(R.string.aiq_moderate),
                resources.getString(R.string.aiq_unhealthy_sensitive),
                resources.getString(R.string.aiq_unhealthy),
                resources.getString(R.string.aiq_very_unhealthy),
                resources.getString(R.string.aiq_hazardous)};

        return descriptions[getIndexForValue(aqi)];
    }

    public static String getEffects(int aqi){
        Resources resources = App.getContext().getResources();
        String[] effects = new String[]{resources.getString(R.string.effects_good),
                resources.getString(R.string.effects_moderate),
                resources.getString(R.string.effects_unhealthy_sensitive),
                resources.getString(R.string.effects_unhealthy),
                resources.getString(R.string.effects_very_unhealthy),
                resources.getString(R.string.effects_hazardous)};

        return effects[getIndexForValue(aqi)];
    }

    public static String getDate(String date, boolean withLastUpdate){
        if (date != ""){
            String fecha = date.split(" ")[0];
            String hora = date.split( " ")[1];
            String dia = fecha.split("-")[2];
            String mes = getMonth(Integer.parseInt( fecha.split( "-")[1])).toLowerCase();

            if (withLastUpdate)
                return String.format( "%s %s %s (%s %s %s)", App.getContext().getResources().getString(R.string.last_update), App.getContext().getResources().getString(R.string.last_update_at), hora, dia, App.getContext().getResources().getString(R.string.last_update_of), mes);
            else
                return String.format( "%s (%s %s %s)", hora, dia, App.getContext().getResources().getString(R.string.last_update_of), mes);
        }
        return "";
    }

    public static String getDate( String date ){
        return getDate( date, true);
    }

    public static String getMonth( int month ){
        Resources resources = App.getContext().getResources();
        String[] months = new String[]{resources.getString(R.string.month1),
                resources.getString(R.string.month2),
                resources.getString(R.string.month3),
                resources.getString(R.string.month4),
                resources.getString(R.string.month5),
                resources.getString(R.string.month6),
                resources.getString(R.string.month7),
                resources.getString(R.string.month8),
                resources.getString(R.string.month9),
                resources.getString(R.string.month10),
                resources.getString(R.string.month11),
                resources.getString(R.string.month12)};

        return months[month-1];
    }

    public static int getPollutionImage(int aqi){
        int[] images = { R.drawable.ic_pollution_good,
                R.drawable.ic_pollution_moderate,
                R.drawable.ic_pollution_unhealthy,
                R.drawable.ic_pollution_unhealthy,
                R.drawable.ic_pollution_very_unhealthy,
                R.drawable.ic_pollution_toxic
        };

        return images[getIndexForValue(aqi)];
    }

    public static String[] getDescriptionsForSettings(){
        Resources resources = App.getContext().getResources();
        return new String[]{
                resources.getString(R.string.aiq_unhealthy_sensitive),
                resources.getString(R.string.aiq_unhealthy),
                resources.getString(R.string.aiq_very_unhealthy),
                resources.getString(R.string.aiq_hazardous)};
    }

    public static int getIndexForValue(int aqi){
        if(aqi < 50)
            return 0;
        else if(aqi < 100)
            return 1;
        else if(aqi < 150)
            return 2;
        else if(aqi < 200)
            return 3;
        else if(aqi < 300)
            return 4;
        else
            return 5;
    }

    public static int getValueForIndex(int index){
        switch (index){
            case 0:
                return 0;
            case 1:
                return 50;
            case 2:
                return 100;
            case 3:
                return 150;
            case 4:
                return 200;
            case 5:
                return 300;
            default:
                return -1;
        }
    }

    private static int getNotificationIcon(){
        return R.drawable.ic_station;
    }
}
