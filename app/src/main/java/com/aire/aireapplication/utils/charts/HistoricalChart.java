package com.aire.aireapplication.utils.charts;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Reading;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miguel on 04/04/2018.
 */

public class HistoricalChart implements OnChartValueSelectedListener {
    BarChart chart;
    ArrayList<BarEntry> entries;
    ArrayList<String> dates;

    public HistoricalChart(BarChart chart, ArrayList<Reading> readings, ArrayList<String> dates, String polluter){
        this.chart = chart;
        this.dates = dates;

        // meter los datos
        entries = new ArrayList<BarEntry>();
        int i = 0;
        for (Reading reading: readings){
            entries.add( new BarEntry(i, reading.getValue(polluter).floatValue() ) );
            i++;
        }
        MyBarDataSet set = new MyBarDataSet( entries, polluter);

        BarData barData = new BarData( set );
        Context context = App.getContext();
        set.setColors(new int[]{ContextCompat.getColor(context, R.color.aiq_good),
                ContextCompat.getColor(context, R.color.aiq_moderate),
                ContextCompat.getColor(context, R.color.aiq_unhealthy_sensitive),
                ContextCompat.getColor(context, R.color.aiq_unhealthy),
                ContextCompat.getColor(context, R.color.aiq_very_unhealthy),
                ContextCompat.getColor(context, R.color.aiq_hazardous)});
        //barData.setDrawValues(false);
        barData.setValueFormatter(new ValueFormatter());
        chart.setData(barData);

        // Quitar la descripcion o poner otra cosa
        Description desc = new Description();
        desc.setText("");
        chart.setDescription(desc);

        // quitar el eje Y de la derecha
        YAxis ejeYdcho = chart.getAxisRight();
        ejeYdcho.setDrawGridLines(false);
        ejeYdcho.setEnabled(false);

        // eje x no gracias
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);

        // poner el valor minimo a 0 y quitar lineas del grid
        AxisBase ejeY = chart.getAxisLeft();
        ejeY.setAxisMinimum(0);
        ejeY.setDrawAxisLine(false);

        // estilos
        chart.setVisibleYRangeMinimum(0, chart.getAxisLeft().getAxisDependency());
        chart.setFitBars(true);
        chart.setDrawBorders(true);
        chart.setDrawGridBackground(false);
        chart.getLegend().setEnabled(false);
        chart.setOnChartValueSelectedListener(this);

        chart.invalidate();
    }

    public BarChart getChart(){
        return chart;
    }

    public BarEntry getEntryForXIndex(int index){
        return entries.get(index);
    }

    public class MyBarDataSet extends BarDataSet {

        public MyBarDataSet(List<BarEntry> yVals, String label) {
            super(yVals, label);
        }

        @Override
        public int getColor(int index) {
            if(getEntryForXIndex(index).getY() < 50)
                return mColors.get(0);
            else if(getEntryForXIndex(index).getY() < 100)
                return mColors.get(1);
            else if(getEntryForXIndex(index).getY() < 150)
                return mColors.get(2);
            else if(getEntryForXIndex(index).getY() < 200)
                return mColors.get(3);
            else if(getEntryForXIndex(index).getY() < 300)
                return mColors.get(4);
            else
                return mColors.get(5);
        }

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Description desc = new Description();
        int x = Math.round(e.getX());

        String date = dates.get(x);
        desc.setText(date);
        desc.setTextSize(14);
        chart.setDescription(desc);
    }

    @Override
    public void onNothingSelected() {
        Description desc = new Description();
        desc.setText("");
        chart.setDescription(desc);
    }
}
