package com.aire.aireapplication.utils.charts;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Reading;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class LatestDataChart {
    BarChart chart;
    ArrayList<BarEntry> entries;

    public LatestDataChart(BarChart chart, Reading reading){
        this.chart = chart;
        chart.clear();

        // meter los datos
        entries = new ArrayList<BarEntry>();

        entries.add( new BarEntry(0, reading.getPm10().intValue()));
        entries.add( new BarEntry(1, reading.getPm25().intValue()));
        entries.add( new BarEntry(2, reading.getSulfurDioxide().intValue()));
        entries.add( new BarEntry(3, reading.getOzone().intValue()));
        //entries.add( new BarEntry(4, reading.getCarbonMonoxide().intValue()));
        entries.add( new BarEntry(4, reading.getNitrogenDioxide().intValue()));

        MyBarDataSet set = new MyBarDataSet( entries, "AQI");
        BarData barData = new BarData( set );
        Context context = App.getContext();
        set.setColors(new int[]{ContextCompat.getColor(context, R.color.aiq_good),
                ContextCompat.getColor(context, R.color.aiq_moderate),
                ContextCompat.getColor(context, R.color.aiq_unhealthy_sensitive),
                ContextCompat.getColor(context, R.color.aiq_unhealthy),
                ContextCompat.getColor(context, R.color.aiq_very_unhealthy),
                ContextCompat.getColor(context, R.color.aiq_hazardous)});
        barData.setValueFormatter(new ValueFormatter());
        chart.setData(barData);

        // poner el eje X debajo
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(getXFormatter());
        xAxis.setGranularity(1);
        xAxis.setDrawGridLines(false);

        // Quitar la descripcion o poner otra cosa
        Description desc = new Description();
        desc.setText("");
        chart.setDescription(desc);
        chart.setDragEnabled(false);

        // quitar el eje Y de la derecha
        YAxis ejeYdcho = chart.getAxisRight();
        ejeYdcho.setDrawGridLines(false);
        ejeYdcho.setEnabled(false);

        // poner el valor minimo a 0 y quitar lineas del grid
        AxisBase ejeY = chart.getAxisLeft();
        ejeY.setAxisMinimum(0);
        ejeY.setDrawAxisLine(false);

        // estilos
        chart.setVisibleYRangeMinimum(0, chart.getAxisLeft().getAxisDependency());
        chart.setFitBars(true);
        chart.setDrawBorders(true);
        chart.setDrawGridBackground(false);
        chart.getLegend().setEnabled(false);
        chart.setTouchEnabled(false);

        chart.invalidate();
    }

    public BarChart getChart(){
        return chart;
    }

    private IAxisValueFormatter getXFormatter(){
        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String[] polluters = new String[] { "PM10", "PM2.5", "SO2", "O3", "NO2" };
                return polluters[(int) value];
            }
        };

        return formatter;
    }

    public BarEntry getEntryForXIndex(int index){
        return entries.get(index);
    }

    public class MyBarDataSet extends BarDataSet {

        public MyBarDataSet(List<BarEntry> yVals, String label) {
            super(yVals, label);
        }

        @Override
        public int getColor(int index) {
            if(getEntryForXIndex(index).getY() < 50)
                return mColors.get(0);
            else if(getEntryForXIndex(index).getY() < 100)
                return mColors.get(1);
            else if(getEntryForXIndex(index).getY() < 150)
                return mColors.get(2);
            else if(getEntryForXIndex(index).getY() < 200)
                return mColors.get(3);
            else if(getEntryForXIndex(index).getY() < 300)
                return mColors.get(4);
            else
                return mColors.get(5);
        }

    }
}
