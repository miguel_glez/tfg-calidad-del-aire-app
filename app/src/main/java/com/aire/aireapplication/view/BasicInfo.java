package com.aire.aireapplication.view;

import android.os.Parcel;
import android.os.Parcelable;

import com.aire.aireapplication.utils.PollutionStaticData;

public class BasicInfo implements Parcelable {

    public int infoID;
    public String name;
    public String maxPolluter;
    public int maxAqi;
    public String description;
    public String effects;
    public int backgroundColor;
    public String date;
    public int image;

    public BasicInfo(int infoID, String name, String maxPolluter, Double maxAqi, String date) {
        super();
        this.infoID = infoID;
        this.name = name;
        if (maxPolluter.equals("pm25")) maxPolluter = "PM2.5";
        this.maxPolluter = maxPolluter;
        this.maxAqi = maxAqi.intValue();

        this.backgroundColor = PollutionStaticData.getColor(maxAqi.intValue());
        this.description = PollutionStaticData.getDescription(maxAqi.intValue());
        this.effects = PollutionStaticData.getEffects(maxAqi.intValue());
        this.date = PollutionStaticData.getDate(date);
        this.image = PollutionStaticData.getPollutionImage(maxAqi.intValue());
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxPolluter(String maxPolluter) {
        this.maxPolluter = maxPolluter;
        update();
    }

    public void setMaxAqi(int maxAqi) {
        this.maxAqi = maxAqi;
        update();
    }

    public void update(){
        this.description = PollutionStaticData.getDescription(maxAqi);
        this.effects = PollutionStaticData.getEffects(maxAqi);
        this.backgroundColor = PollutionStaticData.getColor(maxAqi);
        this.image = PollutionStaticData.getPollutionImage(maxAqi);
        if (maxPolluter.equals("pm25")) maxPolluter = "PM2.5";
    }

    // PARCELAR

    public BasicInfo(Parcel in){
        String[] data = new String[3];

        in.readStringArray(data);
        // the order needs to be the same as in writeToParcel() method
        this.infoID = Integer.valueOf(data[0]);
        this.name = data[1];
        this.maxPolluter = data[2];
        this.maxAqi = Integer.valueOf(data[3]);
        this.description = data[4];
        this.effects = data[5];
        this.backgroundColor = Integer.valueOf(data[6]);
        this.date = data[7];
        this.image = Integer.valueOf(data[8]);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
                String.valueOf(this.infoID),
                this.name,
                this.maxPolluter,
                String.valueOf(this.maxAqi),
                this.description,
                this.effects,
                String.valueOf(this.backgroundColor),
                this.date,
                String.valueOf(this.image)
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public BasicInfo createFromParcel(Parcel in) {
            return new BasicInfo(in);
        }

        public BasicInfo[] newArray(int size) {
            return new BasicInfo[size];
        }
    };
}