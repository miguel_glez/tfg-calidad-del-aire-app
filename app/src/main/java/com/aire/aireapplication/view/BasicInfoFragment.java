package com.aire.aireapplication.view;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aire.aireapplication.R;
import com.aire.aireapplication.databinding.FragmentBasicInfoBinding;


public class BasicInfoFragment extends Fragment {
    FragmentBasicInfoBinding binding;
    BasicInfo info;

    public static BasicInfoFragment newInstance( BasicInfo info ){
        BasicInfoFragment fragment = new BasicInfoFragment();
        fragment.info = info;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_basic_info, container, false);
        View view = binding.getRoot();

        if (info != null) {
            binding.setInformation(info);
        }

        return view;
    }

    public void setInfo( BasicInfo info ){
        this.info = info;
        if (binding != null) {
            binding.setInformation(info);
        }
    }

    public BasicInfo getInfo(){
        return info;
    }
}
