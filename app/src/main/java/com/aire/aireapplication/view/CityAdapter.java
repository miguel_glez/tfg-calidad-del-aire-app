package com.aire.aireapplication.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aire.aireapplication.App;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.PollutionStaticData;

import java.util.List;

/**
 * Created by Miguel on 11/04/2018.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.MyViewHolder> {
    private List<Station> stationList;
    private LayoutInflater inflater;

    public CityAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public CityAdapter(Context context, List<Station> stationList){
        this(context);
        this.stationList = stationList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mStationName;
        TextView mMaxPolluter;
        TextView mAirQuality;
        CardView mCard;

        public MyViewHolder(View itemView) {
            super(itemView);
            mStationName = itemView.findViewById(R.id.city_adapter_station_name);
            mMaxPolluter = itemView.findViewById(R.id.city_adapter_max_polluter);
            mAirQuality = itemView.findViewById(R.id.city_adapter_aqi);
            mCard = itemView.findViewById(R.id.city_adapter_card);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_city_adapter, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Station current = stationList.get(position);
        Reading latestReading = current.getLatestReading();
        String maxPolluter = latestReading.getDominentPolluter().toUpperCase();
        int aqi = latestReading.getAirQuality().intValue();
        if (maxPolluter.equals("PM25")) maxPolluter = "PM2.5";

        holder.mMaxPolluter.setText( String.format( "%s: %s", App.getContext().getResources().getString(R.string.max_polluter), maxPolluter));
        holder.mAirQuality.setText( String.valueOf(aqi) );
        holder.mStationName.setText( current.getName() );
        holder.mCard.setCardBackgroundColor(PollutionStaticData.getColor(aqi));
    }

    @Override
    public int getItemCount() {
        return stationList != null? stationList.size() : 0;
    }

    public void setStationList(List<Station> stationList) {
        this.stationList = stationList;
        notifyDataSetChanged();
    }

    public Station getStation( int index ){
        return stationList.get(index);
    }
}
