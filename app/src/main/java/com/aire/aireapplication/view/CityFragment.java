package com.aire.aireapplication.view;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.utils.RecyclerItemClickListener;
import com.aire.aireapplication.viewmodel.CityViewModel;
import com.aire.aireapplication.viewmodel.factory.CityViewModelFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityFragment extends Fragment {
    int cityID;
    CityViewModel model;

    boolean showLastHourContainer = false;
    int stationSelected = 0;

    RecyclerView recyclerView;
    BasicInfoFragment basicInfoFragment;

    public static CityFragment newInstance(int cityID){
        CityFragment fragment = new CityFragment();
        fragment.cityID = cityID;

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        this.cityID = (savedInstanceState != null? savedInstanceState.getInt("cityID") : this.cityID);
        this.model = ViewModelProviders.of(this, new CityViewModelFactory(getActivity().getApplication(), this.cityID)).get(CityViewModel.class);

        Log.i(Keys.CITY_FRAGMENT_TAG, "MODEL CREADO? "+ (this.model!= null) );

        final Observer<City> cityObserver = new Observer<City>() {
            @Override
            public void onChanged(@Nullable City city) {
                if (city!=null) {
                    BasicInfo basicInfo = new BasicInfo(city.getId(),
                            city.getName(),
                            model.getMaxPolluter(),
                            model.getMaxAqi()*1.0,
                            "");

                    // cambiamos el texto de la barra superior
                    ((MainActivity) getActivity()).setTopBarTitle(city.getName());
                    ((MainActivity) getActivity()).setCityName(city.getName());

                    basicInfoFragment.setInfo(basicInfo);
                }
            }
        };
        model.getCity().observe(this, cityObserver);

        final Observer<List<Station>> stationsObserver = new Observer<List<Station>>() {
            @Override
            public void onChanged(@Nullable final List<Station> stations) {
                ((CityAdapter)recyclerView.getAdapter()).setStationList(stations);

                BasicInfo info =  basicInfoFragment.getInfo();
                if (info != null) {
                    info.setMaxPolluter( model.getMaxPolluter() );
                    info.setMaxAqi( model.getMaxAqi() );
                }
                basicInfoFragment.setInfo(info);
            }
        };
        model.getStations().observe(this, stationsObserver);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("cityID", this.cityID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_city, container, false);

        // flag para saber que hacer en caso de click
        this.showLastHourContainer = (fragmentView.findViewById(R.id.city_last_hour_container) != null);
        this.stationSelected = 0;

        // importante para que nos cargue nuestro botoncito arriba
        setHasOptionsMenu(true);

        // no queremos boton de vuelta
        ((MainActivity)getActivity()).showBackButton(false);

        if (savedInstanceState != null){
            this.cityID = savedInstanceState.getInt("cityID");
        }
        FragmentManager fm = getFragmentManager();

        recyclerView = fragmentView.findViewById(R.id.city_recycler_view);
        CityAdapter adapter = new CityAdapter(getContext(), new ArrayList<Station>());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {FragmentManager fm = getFragmentManager();
            @Override
            public void onItemClick(View view, int position) {
                Station station = ((CityAdapter) (recyclerView.getAdapter())).getStation(position);

                Log.i(Keys.CITY_FRAGMENT_TAG,"Hay container: "+showLastHourContainer );
                if (showLastHourContainer){
                    // colocamos el fragmento con la gráfica
                    fm.beginTransaction()
                            .replace(R.id.city_last_hour_container, LastHourFragment.newInstance(station.getLatestReading()), Keys.LAST_HOUR_FRAGMENT_TAG)
                            .commit();

                    // marcamos como pulsado el que queramos y desmarcamos lo demás
                    for (int i=0; i<recyclerView.getAdapter().getItemCount(); i++){
                        recyclerView.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    }
                    view.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    stationSelected = station.getStationID();
                }else
                    ((MainActivity) getActivity()).setStationFragment(station.getId());
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // nada
            }
        }));

        basicInfoFragment = BasicInfoFragment.newInstance(null);

        fm.beginTransaction()
                .replace(R.id.city_basic_info_container, basicInfoFragment, Keys.BASIC_INFO_FRAGMENT_TAG)
                .commit();

        return fragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.toolbar_city, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        FragmentManager fm = getFragmentManager();
        if (item.getItemId() == R.id.toolbar_city_select){
            CitySelectorFragment fragment = CitySelectorFragment.newInstance(this, true);

            ((MainActivity) getActivity()).updateFragmentInfo(Keys.CITY_SELECTOR_FRAGMENT_TAG);

            fm.beginTransaction()
                    .replace(R.id.fragment_container, fragment, Keys.CITY_SELECTOR_FRAGMENT_TAG)
                    .addToBackStack(null)
                    .commit();
        }
        else if(item.getItemId() == R.id.toolbar_city_historical){
            if (stationSelected > 0){
                HistoricalFragment fragment = HistoricalFragment.newInstance(stationSelected);

                ((MainActivity) getActivity()).updateFragmentInfo(Keys.HISTORICAL_FRAGMENT_TAG);
                fm.beginTransaction()
                        .replace(R.id.fragment_container, fragment, Keys.HISTORICAL_FRAGMENT_TAG)
                        .addToBackStack(null)
                        .commit();
            }else
                Toast.makeText(getContext(), getResources().getString(R.string.select_station), Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
