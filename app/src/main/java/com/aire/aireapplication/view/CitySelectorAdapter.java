package com.aire.aireapplication.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.viewmodel.CitySelectorViewModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Miguel on 08/05/2018.
 */

public class CitySelectorAdapter extends RecyclerView.Adapter<CitySelectorAdapter.MyViewHolder> implements TextView.OnClickListener, CheckBox.OnCheckedChangeListener {

    private final SortedList<City> cityList = new SortedList<>(City.class, new SortedList.Callback<City>() {
        @Override
        public int compare(City a, City b) {
            return mComparator.compare(a, b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(City oldItem, City newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(City item1, City item2) {
            return item1.getId() == item2.getId();
        }
    });
    private LayoutInflater inflater;
    private CitySelectorFragment fragment;
    private Comparator<City> mComparator;
    private CitySelectorViewModel vModel;

    public CitySelectorAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public CitySelectorAdapter(CitySelectorFragment fragment, Comparator<City> mComparator){
        this(fragment.getContext());
        this.fragment = fragment;
        this.mComparator = mComparator;
        this.vModel = ViewModelProviders.of(fragment).get(CitySelectorViewModel.class);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mCityName;
        CheckBox mCheckbox;

        public MyViewHolder(View itemView) {
            super(itemView);
            mCityName = itemView.findViewById(R.id.city_selector_adapter_name);
            mCheckbox = itemView.findViewById(R.id.city_selector_adapter_checkbox);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_city_selector_adapter, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        City current = cityList.get(position);

        holder.mCityName.setText( current.getName() );
        holder.mCityName.setOnClickListener(this);

        holder.mCheckbox.setTag(position);
        holder.mCheckbox.setChecked( current.isFavorite() );
        holder.mCheckbox.setOnCheckedChangeListener(this);
    }

    @Override
    public int getItemCount() {
        return cityList != null? cityList.size() : 0;
    }

    public void setCityList(List<City> cityList) {
        replaceAll(cityList);
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        TextView text = (TextView) v;
        City city = this.getCity((String) text.getText());
        ((MainActivity) fragment.getActivity()).setCityFragment(city);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int index = (int) buttonView.getTag();
        City city = getCity( index );

        // por temas del recycler view, puede que no haga falta actualizar
        if (isChecked != city.isFavorite()) {
            // primero (IMPORTANTISIMO) eliminamos del adapter la entrada
            remove(city);

            // luego lo marcamos como favorito y lo mandamos a actualizar a la bd
            city.setFavorite(isChecked);

            vModel.updateCity(city);

            // log
            Log.i(Keys.CITY_SELECTOR_FRAGMENT_TAG + " ADAPTER", city.getName() + (isChecked) + buttonView.getTag());
        }
    }

    public City getCity( String name ){
        for (int i=0; i<cityList.size(); i++){
            City city = cityList.get(i);
            if (city.getName().equals(name))
                return city;
        }
        return null;
    }

    public City getCity( int index ){
        return this.cityList.get(index);
    }

    public void add(City model) {
        cityList.add(model);
    }

    public void add(List<City> models) {
        cityList.addAll(models);
    }

    public void remove(City model) {
        cityList.remove(model);
    }

    public void remove(List<City> models) {
        cityList.beginBatchedUpdates();
        for (City model : models) {
            cityList.remove(model);
        }
        cityList.endBatchedUpdates();
    }

    public void replaceAll(List<City> models) {
        cityList.beginBatchedUpdates();
        for (int i = cityList.size() - 1; i >= 0; i--) {
            final City model = cityList.get(i);
            if (!models.contains(model)) {
                cityList.remove(model);
            }
        }
        cityList.addAll(models);
        cityList.endBatchedUpdates();
    }
}