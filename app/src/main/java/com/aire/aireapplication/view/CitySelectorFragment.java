package com.aire.aireapplication.view;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.utils.CityComparator;
import com.aire.aireapplication.viewmodel.CitySelectorViewModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CitySelectorFragment extends Fragment implements SearchView.OnQueryTextListener{
    CitySelectorViewModel model;
    CityFragment parent;
    boolean showBackButton;
    RecyclerView recyclerView;
    CitySelectorAdapter mAdapter;
    Comparator<City> mComparator;

    public CitySelectorFragment() {}

    public static CitySelectorFragment newInstance(CityFragment parent, boolean showBackButton) {
        CitySelectorFragment fragment = new CitySelectorFragment();
        fragment.parent = parent;
        fragment.showBackButton = showBackButton;

        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("showBackButton", this.showBackButton );
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_selector, container, false);
        this.model = ViewModelProviders.of(this).get(CitySelectorViewModel.class);
        setHasOptionsMenu(true);

        if (savedInstanceState != null){
            this.showBackButton = savedInstanceState.getBoolean("showBackButton");
        }

        // boton de ir atrás
        ((MainActivity)getActivity()).showBackButton(this.showBackButton);

        // título
        ((MainActivity) getActivity()).setTopBarTitle( getResources().getString(R.string.select_city) );

        // cargar la lista de ciudades
        mComparator = CityComparator.getComparator();
        recyclerView = view.findViewById(R.id.city_selector_recycler_view);
        mAdapter = new CitySelectorAdapter(this, mComparator);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);

        final Observer<List<City>> cityObserver = new Observer<List<City>>() {
            @Override
            public void onChanged(@Nullable final List<City> cities) {
                ((CitySelectorAdapter)recyclerView.getAdapter()).setCityList(cities);
            }
        };
        model.getCities().observe(this, cityObserver);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_city_selector, menu);

        final MenuItem searchItem = menu.findItem(R.id.toolbar_city_selector_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        super.onCreateOptionsMenu(menu, inflater);
    }

    private static List<City> filter(List<City> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<City> filteredModelList = new ArrayList<>();
        for (City model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<City> filteredModelList = filter(model.getCities().getValue(), query);
        mAdapter.replaceAll(filteredModelList);
        recyclerView.scrollToPosition(0);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
