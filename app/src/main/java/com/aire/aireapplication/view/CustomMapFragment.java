package com.aire.aireapplication.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.utils.PollutionStaticData;
import com.aire.aireapplication.viewmodel.MapViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Miguel on 05/04/2018.
 */

public class CustomMapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    GoogleMap readyMap;
    MapViewModel model;
    HashMap<Integer, Marker> markers = new HashMap<>();

    public static CustomMapFragment newInstance(){
        CustomMapFragment fragment = new CustomMapFragment();
        fragment.getMapAsync(fragment);

        return fragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        readyMap = googleMap;
        readyMap.setOnInfoWindowClickListener(this);

        final Observer<List<Station>> stationsObserver = new Observer<List<Station>>() {
            @Override
            public void onChanged(@Nullable List<Station> stations) {
                LatLng pos = null;
                int aqi;
                for (Station station : stations) {
                    Marker marker = markers.get(station.getId());

                    if (marker != null){
                        if (marker.isVisible()){
                            marker.remove();
                            markers.remove(station.getId());
                        }
                    }

                    pos = new LatLng(station.getLatitude(), station.getLongitude());
                        //Log.i("ADD_MARKER", String.format( "%s: %f", station.getName(), station.getLatestReading().getAirQuality().floatValue()));
                    aqi = station.getLatestReading().getAirQuality().intValue();

                    Drawable markerPin = getResources().getDrawable(PollutionStaticData.getMarker( aqi ));

                    marker = readyMap.addMarker(new MarkerOptions()
                            .position(pos)
                            .title(station.getName())
                            .snippet(getResources().getString(R.string.map_click_to_see))
                            //.snippet(String.format( "%s: %d", getResources().getString(R.string.aiq_text), aqi ))
                            .icon(getMarkerIconFromDrawable(markerPin)) );
                            //.icon(BitmapDescriptorFactory.defaultMarker(PollutionStaticData.getMarkerColor( aqi ))));
                    markers.put( station.getId(), marker );

                }
                if (pos!=null) {
                    readyMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 8));
                }
            }
        };
        this.model.getStations().observe(this, stationsObserver);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        this.model = ViewModelProviders.of(this).get(MapViewModel.class);
        if (bundle != null){
            this.getMapAsync(this);
            //((MainActivity)this.getActivity()).setTopBarTitle(getResources().getString(R.string.map));
        }

        super.onActivityCreated(bundle);
    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        String stationName = marker.getTitle();
        Log.i(Keys.MAP_FRAGMENT_TAG, "WINDOW INFO CLICK");
        for (Station station: model.getStations().getValue()){
            if (station.getName().equals(stationName)){
                Log.i(Keys.MAP_FRAGMENT_TAG, "STATION ID: "+ station.getStationID());
                ((MainActivity) getActivity()).setStationFragment(station.getStationID());
            }

        }
    }

    private static BitmapDescriptor getMarkerIconFromDrawable( Drawable drawable ){
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
