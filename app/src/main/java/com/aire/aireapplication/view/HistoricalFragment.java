package com.aire.aireapplication.view;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.network.FetchOperations;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.utils.PollutionStaticData;
import com.aire.aireapplication.utils.charts.HistoricalChart;
import com.aire.aireapplication.viewmodel.HistoricalViewModel;
import com.aire.aireapplication.viewmodel.factory.HistoricalViewModelFactory;
import com.github.mikephil.charting.charts.BarChart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoricalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class HistoricalFragment extends Fragment {
    int stationID;
    String operation;
    public HistoricalViewModel model;
    BarChart chart;
    ArrayList<Reading> updatedReadings = new ArrayList<>();
    String polluter = "AQI";
    int entries = 0;

    public HistoricalFragment() {}

    public static HistoricalFragment newInstance(int stationID) {
        HistoricalFragment fragment = new HistoricalFragment();
        fragment.stationID = stationID;

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        this.stationID = (savedInstanceState != null? savedInstanceState.getInt("stationID") : this.stationID);
        this.model = ViewModelProviders.of(this, new HistoricalViewModelFactory(getActivity().getApplication(), this.stationID)).get(HistoricalViewModel.class);

        if (savedInstanceState != null) {
            restoreData(savedInstanceState);
        }

        super.onActivityCreated(savedInstanceState);
    }

    public void restoreData(Bundle savedInstanceState) {
        this.operation = savedInstanceState.getString("operation");
        this.stationID = savedInstanceState.getInt("stationID");

        switchDatesInChart( this.operation.equals(FetchOperations.STATION_GET_LAST_DAY)? 0 : 1, true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("operation", this.operation);
        outState.putInt("stationID", this.stationID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historical, container, false);
        chart = view.findViewById(R.id.historical_chart);

        configPolluterDropdown((Spinner) view.findViewById(R.id.historical_spinner));
        configDateDropdown((Spinner) view.findViewById(R.id.historical_spinner_dates));

        ((MainActivity)getActivity()).showBackButton(true);
        // cambiamos el texto de la barra superior
        ((MainActivity) getActivity()).setTopBarTitle(getContext().getResources().getString(R.string.historical));

        return view;
    }


    private void switchPolluterInChart(String polluter){
        Collections.sort(updatedReadings);
        Collections.reverse(updatedReadings);

        this.polluter = polluter;

        ArrayList<String> dates = new ArrayList<>();
        for (Reading reading : updatedReadings) {
            dates.add(PollutionStaticData.getDate(reading.getDate(), false));
        }

        new HistoricalChart(chart, updatedReadings, dates, polluter);
    }

    private void configPolluterDropdown(Spinner spinner){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switchPolluterInChart( (String) parent.getAdapter().getItem(position) );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // lista de datos al adapter con un layout hecho ya por android
        ArrayAdapter <String> adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, getContext().getResources().getStringArray(R.array.polluters));

        // dropdown como estilo: list view con radio buttons
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // le metemos el adapter al settings_min_polluter_spinner
        spinner.setAdapter( adapter );
    }

    public void switchDatesInChart(int position){
        switchDatesInChart(position, false);
    }

    public void switchDatesInChart(int position, boolean restoring){
        String newOp;
        if (position == 0)
            newOp = FetchOperations.STATION_GET_LAST_DAY;
        else if(position == 1)
            newOp = FetchOperations.STATION_GET_LAST_WEEK;
        else
            return;

        if (!newOp.equals(this.operation) || restoring){
            this.operation = newOp;

            //Log.i(Keys.HISTORICAL_FRAGMENT_TAG, "RESTORING: MODEL "+ (model!=null) );
            model.switchData(operation);

            final Observer<List<Reading>> readingsObserver = new Observer<List<Reading>>() {
                @Override
                public void onChanged(@Nullable List<Reading> readings) {
                    if (readings.size()> entries) {
                        entries = readings.size();
                        updatedReadings = new ArrayList<>(readings);
                        //Log.i(Keys.HISTORICAL_FRAGMENT_TAG, "READINGS CHANGED: " + updatedReadings.size());

                        ArrayList<String> dates = new ArrayList<>();
                        for (Reading reading : updatedReadings) {
                            dates.add(PollutionStaticData.getDate(reading.getDate(), false));
                        }
                        new HistoricalChart(chart, updatedReadings, dates, polluter);

                        // es cutre vale? pero arregla un error
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                entries = 0;
                            }
                        }, 1500);
                    }
                }
            };
            model.getReadings().observe(this, readingsObserver);
        }
    }

    private void configDateDropdown(Spinner spinner){
        final String[] values = {getContext().getResources().getString(R.string.last_day_data), getContext().getResources().getString(R.string.last_week_data) };

        // lista de datos al adapter con un layout hecho ya por android
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, values);

        // dropdown como estilo: list view con radio buttons
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (operation == null){
            spinner.setSelection(0);
        }else {
            entries = 0;
            if (operation.equals(FetchOperations.STATION_GET_LAST_DAY))
                spinner.setSelection(0);
            else if (operation.equals(FetchOperations.STATION_GET_LAST_WEEK))
                spinner.setSelection(1);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switchDatesInChart( position );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // le metemos el adapter al settings_min_polluter_spinner
        spinner.setAdapter( adapter );
    }

}
