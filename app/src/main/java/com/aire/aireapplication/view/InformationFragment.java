package com.aire.aireapplication.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.aire.aireapplication.R;

public class InformationFragment extends Fragment {

    public static InformationFragment newInstance(){
        InformationFragment fragment = new InformationFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information, container, false);

        TabHost host = view.findViewById(R.id.information_tab_host);
        host.setup();

        // tab aqi
        TabHost.TabSpec spec = host.newTabSpec(getResources().getString(R.string.aqi));
        spec.setContent(R.id.aqi_tab);
        spec.setIndicator(getResources().getString(R.string.aqi));
        host.addTab(spec);

        // tab polluters
        spec = host.newTabSpec(getResources().getString(R.string.polluters));
        spec.setContent(R.id.polluter_tab);
        spec.setIndicator(getResources().getString(R.string.polluters));
        host.addTab(spec);

        return view;
    }

}
