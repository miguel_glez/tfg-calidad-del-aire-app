package com.aire.aireapplication.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.utils.PollutionStaticData;
import com.aire.aireapplication.utils.charts.LatestDataChart;
import com.github.mikephil.charting.charts.BarChart;

public class LastHourFragment extends Fragment {
    Reading latestReading;

    BarChart chart;
    TextView dateTextView;

    public static LastHourFragment newInstance( Reading latestReading ){
        LastHourFragment fragment = new LastHourFragment();
        fragment.latestReading = latestReading;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_last_hour, container, false);
        chart = fragmentView.findViewById(R.id.last_hour_chart);
        dateTextView = fragmentView.findViewById(R.id.last_hour_updated_at);

        if (this.latestReading != null){
            new LatestDataChart(this.chart, latestReading);
            this.dateTextView.setText(PollutionStaticData.getDate(latestReading.getDate()));
        }

        return fragmentView;
    }

    public Reading getLatestReading() {
        return latestReading;
    }

    public void setLatestReading(Reading latestReading) {
        this.latestReading = latestReading;
        if (this.chart != null && this.dateTextView != null) {
            new LatestDataChart(this.chart, latestReading);
            this.dateTextView.setText(PollutionStaticData.getDate(latestReading.getDate()));
        }
    }
}
