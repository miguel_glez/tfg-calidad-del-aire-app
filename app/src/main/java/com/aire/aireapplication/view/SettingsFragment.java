package com.aire.aireapplication.view;


import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.aire.aireapplication.R;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.utils.PollutionStaticData;
import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;

import static android.content.Context.MODE_PRIVATE;

public class SettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, ColorPickerDialogFragment.ColorPickerDialogListener {
    private Switch notificationSwitch;
    private Switch vibrationSwitch;
    private Switch soundSwitch;
    private Spinner alertLevelSpinner;
    private ImageView colorButton;

    public static SettingsFragment newInstance(){
        SettingsFragment fragment = new SettingsFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        // cargamos las preferencias de las notificaciones
        SharedPreferences notificationPref = getActivity().getApplicationContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);

        boolean notifications = notificationPref.getBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, true); // por defecto las activamos
        boolean vibration = notificationPref.getBoolean(Keys.PREF_VIBRATION_ENABLED, false); // por defecto desactivada
        boolean sound = notificationPref.getBoolean(Keys.PREF_SOUND_ENABLED, false); // por defecto desactivado
        int alertLevel = notificationPref.getInt(Keys.PREF_ALERT_LEVEL, 0);

        notificationSwitch = view.findViewById(R.id.notifications_switch);
        notificationSwitch.setOnCheckedChangeListener(this);

        vibrationSwitch = view.findViewById(R.id.vibration_switch);
        vibrationSwitch.setOnCheckedChangeListener(this);

        soundSwitch = view.findViewById(R.id.sound_switch);
        soundSwitch.setOnCheckedChangeListener(this);

        // preparamos el color picker para el color de las notificaciones
        colorButton = view.findViewById(R.id.color_circle);

        setColorButtonNewColor(notificationPref.getInt(Keys.PREF_LED_COLOR, Keys.DEFAULT_LED_COLOR));

        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notificationSwitch.isChecked()) {
                    // es posible que haya cambiado
                    SharedPreferences currentSharedPref = getActivity().getApplicationContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);

                    // no funciona con versiones menores
                    if (Build.VERSION.SDK_INT >= 15) {
                        ColorPickerDialogFragment f = ColorPickerDialogFragment.newInstance(0, getResources().getString(R.string.choose_color), null, currentSharedPref.getInt(Keys.PREF_LED_COLOR, Keys.DEFAULT_LED_COLOR), false);
                        f.setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog);
                        f.show(getActivity().getFragmentManager(), "COLOR_PICKER");
                    }
                }
            }
        });

        alertLevelSpinner = view.findViewById(R.id.notification_settings_spinner);
        alertLevelSpinner.setAdapter(new ArrayAdapter<>(this.getContext(),android.R.layout.simple_spinner_dropdown_item, PollutionStaticData.getDescriptionsForSettings() ));
        alertLevelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences notificationPref = getActivity().getApplicationContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
                SharedPreferences.Editor editor = notificationPref.edit();

                editor.putInt(Keys.PREF_ALERT_LEVEL, position);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        alertLevelSpinner.setSelection(alertLevel);

        notificationSwitch.setChecked(notifications);
        if (notifications) {
            vibrationSwitch.setChecked(vibration);
            soundSwitch.setChecked(sound);
        }else{
            vibrationSwitch.setEnabled(false);
            soundSwitch.setEnabled(false);
            alertLevelSpinner.setEnabled(false);
            setColorButtonNewColor(Color.parseColor("#999999"));
        }

        return view;
    }

    private void setColorButtonNewColor(int color){
        Drawable colorCircle = getResources().getDrawable(R.drawable.ic_circle);
        colorCircle.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        colorButton.setImageDrawable(colorCircle);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences notificationPref = getActivity().getApplicationContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = notificationPref.edit();
        Switch switchBtn = (Switch) buttonView;

        switch(switchBtn.getId()){
            case R.id.notifications_switch:
                editor.putBoolean(Keys.PREF_NOTIFICATIONS_ENABLED, isChecked);
                vibrationSwitch.setEnabled(isChecked);
                soundSwitch.setEnabled(isChecked);
                alertLevelSpinner.setEnabled(isChecked);
                if (isChecked)
                    setColorButtonNewColor(notificationPref.getInt(Keys.PREF_LED_COLOR, Keys.DEFAULT_LED_COLOR));
                else
                    setColorButtonNewColor(Color.parseColor("#999999"));

                break;
            case R.id.vibration_switch:
                editor.putBoolean(Keys.PREF_VIBRATION_ENABLED, isChecked);

                break;
            case R.id.sound_switch:
                editor.putBoolean(Keys.PREF_SOUND_ENABLED, isChecked);

                break;
        }
        editor.commit();
    }


    @Override
    public void onDialogDismissed(int dialogId) {}

    @Override
    public void onColorSelected(int dialogId, int color) {
        SharedPreferences sharedPref = getActivity().getApplicationContext().getSharedPreferences(Keys.PREF_NOTIFICATION_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Keys.PREF_LED_COLOR, color);
        editor.commit();

        setColorButtonNewColor(color);
    }
}
