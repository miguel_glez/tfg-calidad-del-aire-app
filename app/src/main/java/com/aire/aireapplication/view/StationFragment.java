package com.aire.aireapplication.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.aire.aireapplication.MainActivity;
import com.aire.aireapplication.R;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.utils.Keys;
import com.aire.aireapplication.viewmodel.StationViewModel;
import com.aire.aireapplication.viewmodel.factory.StationViewModelFactory;

public class StationFragment extends Fragment {
    StationViewModel model;

    BasicInfoFragment basicInfoFragment;
    LastHourFragment lastHourFragment;

    int stationID;

    public static StationFragment newInstance(int stationID){
        StationFragment fragment = new StationFragment();
        fragment.stationID = stationID;

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        this.stationID = (savedInstanceState != null? savedInstanceState.getInt("stationID") : this.stationID);
        this.model = ViewModelProviders.of(this, new StationViewModelFactory(getActivity().getApplication(), this.stationID)).get(StationViewModel.class);

        final Observer<Station> stationObserver = new Observer<Station>() {
            @Override
            public void onChanged(@Nullable Station station) {
                if (station!= null) {
                    Reading latestReading = station.getLatestReading();

                    BasicInfo basicInfo = new BasicInfo(station.getId(),
                            station.getName(),
                            latestReading.getDominentPolluter(),
                            latestReading.getAirQuality(),
                            latestReading.getDate());

                    // cambiamos el texto de la barra superior
                    ((MainActivity) getActivity()).setTopBarTitle(station.getName());

                    basicInfoFragment.setInfo(basicInfo);
                    lastHourFragment.setLatestReading(latestReading);
                }
            }
        };
        model.getStation().observe(this, stationObserver);


        super.onActivityCreated(savedInstanceState);
    }

    public void restoreData(Bundle savedInstanceState) {
        this.stationID = savedInstanceState.getInt("stationID");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("stationID", this.stationID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_station, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).showBackButton(true);

        if (savedInstanceState != null){
            restoreData(savedInstanceState);
        }

        basicInfoFragment = BasicInfoFragment.newInstance(null);
        lastHourFragment = LastHourFragment.newInstance(null);

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.station_basic_info_container, basicInfoFragment, Keys.BASIC_INFO_FRAGMENT_TAG)
                .commit();

        fm.beginTransaction()
                .replace(R.id.station_last_hour_container, lastHourFragment, Keys.LAST_HOUR_FRAGMENT_TAG)
                .commit();

        return fragmentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.toolbar_station, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.toolbar_station_historical){
            HistoricalFragment fragment = HistoricalFragment.newInstance(this.stationID);

            ((MainActivity) getActivity()).updateFragmentInfo(Keys.HISTORICAL_FRAGMENT_TAG);
            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
                    .replace(R.id.fragment_container, fragment, Keys.HISTORICAL_FRAGMENT_TAG)
                    .addToBackStack(null)
                    .commit();
        }
        return true;
    }

}
