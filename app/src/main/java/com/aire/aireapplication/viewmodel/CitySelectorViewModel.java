package com.aire.aireapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.repository.AireRepository;

import java.util.List;

/**
 * Created by Miguel on 08/05/2018.
 */

public class CitySelectorViewModel extends AndroidViewModel {
    AireRepository repository;
    LiveData<List<City>> mCities;

    public CitySelectorViewModel(Application application){
        super(application);
        this.repository = new AireRepository(application);

        mCities = repository.getAllCities();
    }

    public void updateCity(City city){
        repository.updateCity(city);
    }

    public LiveData<List<City>> getCities() {
        return mCities;
    }
}
