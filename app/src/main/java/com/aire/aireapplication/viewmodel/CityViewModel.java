package com.aire.aireapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.repository.AireRepository;

import java.util.List;

/**
 * Created by Miguel on 10/04/2018.
 */

public class CityViewModel extends AndroidViewModel {
    AireRepository repository;

    LiveData<City> mCity;
    LiveData<List<Station>> mStations;

    public CityViewModel(Application application, int cityID){
        super(application);
        this.repository = new AireRepository(application);

        mCity = repository.getCity(cityID);
        mStations = repository.getCityStations(cityID);
    }

    public LiveData<City> getCity() {return mCity;}
    public LiveData<List<Station>> getStations() {
        return mStations;
    }

    public String getMaxPolluter(){
        if (this.mStations!=null && this.mStations.getValue()!=null ) {
            if (!this.mStations.getValue().isEmpty()) {
                return calculateMaxPolluter(this.mStations.getValue());
            }
        }
        return "";
    }

    public static String calculateMaxPolluter(List<Station> stations){
        if (stations != null && stations.size()>0) {
            Station maxPollutedStation = stations.get(0);
            for (Station station : stations) {
                if (station.getLatestReading().getAirQuality() > maxPollutedStation.getLatestReading().getAirQuality())
                    maxPollutedStation = station;
            }
            return maxPollutedStation.getLatestReading().getDominentPolluter();
        }
        return null;
    }

    public int getMaxAqi(){
        if (this.mStations!=null && this.mStations.getValue()!=null ) {
            if (!this.mStations.getValue().isEmpty()) {
                return calculateMaxAqi(this.mStations.getValue());
            }
        }
        return 0;
    }

    public static int calculateMaxAqi(List<Station> stations){
        if (stations != null && stations.size() > 0) {
            Station maxPollutedStation = stations.get(0);
            for (Station station : stations) {
                if (station.getLatestReading().getAirQuality() > maxPollutedStation.getLatestReading().getAirQuality())
                    maxPollutedStation = station;
            }
            return maxPollutedStation.getLatestReading().getAirQuality().intValue();
        }
        return 0;
    }
}
