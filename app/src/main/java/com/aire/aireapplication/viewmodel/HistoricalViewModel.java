package com.aire.aireapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aire.aireapplication.network.FetchOperations;
import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.repository.AireRepository;

import java.util.List;

/**
 * Created by Miguel on 04/04/2018.
 */

public class HistoricalViewModel extends AndroidViewModel {
    AireRepository repository;
    LiveData<List<Reading>> readings;
    String operation;
    int stationID;

    public HistoricalViewModel(Application application, int stationID){
        super(application);
        this.repository = new AireRepository(application);

        this.stationID = stationID;
        this.readings = repository.getLastDayReadings(stationID);
        this.operation = FetchOperations.STATION_GET_LAST_DAY;
    }

    public void switchData(String operation){
        if (!operation.equals(this.operation)){
            this.operation = operation;

            if (operation.equals(FetchOperations.STATION_GET_LAST_WEEK))
                this.readings = repository.getLastWeekReadings(stationID);
            else
                this.readings = repository.getLastDayReadings(stationID);
        }
    }


    public LiveData<List<Reading>> getReadings() {
        return readings;
    }
}
