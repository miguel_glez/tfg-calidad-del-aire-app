package com.aire.aireapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.repository.AireRepository;

import java.util.List;

/**
 * Created by Miguel on 05/04/2018.
 */

public class MapViewModel extends AndroidViewModel {
    AireRepository repository;
    LiveData<List<Station>> mStations;

    public MapViewModel(Application application){
        super(application);
        this.repository = new AireRepository(application);

        mStations = repository.getAllStations();
    }

    public LiveData<List<Station>> getStations() {
        return mStations;
    }
}
