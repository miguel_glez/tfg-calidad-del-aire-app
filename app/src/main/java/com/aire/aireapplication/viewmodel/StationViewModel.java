package com.aire.aireapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.repository.AireRepository;

/**
 * Created by Miguel on 18/04/2018.
 */

public class StationViewModel extends AndroidViewModel {
    AireRepository repository;
    LiveData<Station> mStation;

    public StationViewModel(Application application, int stationID){
        super(application);
        this.repository = new AireRepository(application);

        mStation = repository.getStation(stationID);
    }

    public LiveData<Station> getStation() {return mStation;}

}
