package com.aire.aireapplication.viewmodel.factory;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.aire.aireapplication.viewmodel.CityViewModel;

/**
 * Created by Miguel on 19/04/2018.
 */

public class CityViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private Application mApplication;
    private int mParam;


    public CityViewModelFactory(Application application, int param) {
        mApplication = application;
        mParam = param;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CityViewModel(mApplication, mParam);
    }
}