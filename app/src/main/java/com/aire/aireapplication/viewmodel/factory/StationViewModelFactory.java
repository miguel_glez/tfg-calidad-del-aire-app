package com.aire.aireapplication.viewmodel.factory;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.aire.aireapplication.viewmodel.StationViewModel;

/**
 * Created by Miguel on 19/04/2018.
 */

public class StationViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private Application mApplication;
    private int mParam;


    public StationViewModelFactory(Application application, int param) {
        mApplication = application;
        mParam = param;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new StationViewModel(mApplication, mParam);
    }
}