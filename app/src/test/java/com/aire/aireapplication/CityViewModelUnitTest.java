package com.aire.aireapplication;

import com.aire.aireapplication.persistence.domain.Reading;
import com.aire.aireapplication.persistence.domain.Station;
import com.aire.aireapplication.viewmodel.CityViewModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class CityViewModelUnitTest {
    List<Station> stations;

    @Before
    public void populateList(){
        stations = new ArrayList<Station>();

        Station sA = new Station(10000, "stationA", 43.34403,-5.97372, 1);
        Station sB = new Station(10001, "stationB", 43.54898,-5.89722, 1);
        Station sC = new Station(10002, "stationC", 43.55881, -5.92788, 1);
        Station sD = new Station(10003, "stationD", 43.55428,-5.91823, 1);
        Station sE = new Station(10004, "stationE", 43.57532,-5.95849, 1);
        Station sF = new Station(10005, "stationF", 43.55428,-5.95849, 1);

        Reading rLA = new Reading(sA, new Date().getTime(), "PM25", 12.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 12.0, 7.0, 0.0);
        Reading rLB = new Reading(sB, new Date().getTime(), "PM25", 20.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 20.0, 7.0, 0.0);
        Reading rLC = new Reading(sC, new Date().getTime(), "PM25", 10.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 10.0, 7.0, 0.0);
        Reading rLD = new Reading(sD, new Date().getTime(), "PM25", 35.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 35.0, 7.0, 0.0);
        Reading rLE = new Reading(sE, new Date().getTime(), "PM25", 50.0, 0.0, 0.0, 10.0, 5.0, 0.0, 10.0, 50.0, 7.0, 0.0);
        Reading rLF = new Reading(sF, new Date().getTime(), "PM10", 18.0, 0.0, 0.0, 10.0, 5.0, 0.0, 18.0, 15.0, 7.0, 0.0);

        sA.setLatestReading(rLA);
        sB.setLatestReading(rLB);
        sC.setLatestReading(rLC);
        sD.setLatestReading(rLD);
        sE.setLatestReading(rLE);
        sF.setLatestReading(rLF);

        stations.add(sA);
        stations.add(sB);
        stations.add(sC);
        stations.add(sD);
        stations.add(sE);
        stations.add(sF);
    }

    // CVM-001
    @Test
    public void calculateMaxAqi_isCorrect(){
        // la más contaminada tiene índice 50 (station E)
        assertEquals( 50, CityViewModel.calculateMaxAqi(stations) );

        // comprobamos si le metemos un nulo
        assertEquals( 0, CityViewModel.calculateMaxAqi(null) );

        // comprobamos si le metemos una lista vacía
        assertEquals( 0, CityViewModel.calculateMaxAqi( new ArrayList<Station>()));
    }

    // CVM-002
    @Test
    public void calculateMaxPolluter_isCorrect(){
        // la más contaminada tiene índice 50 (station E)
        assertEquals( "PM2.5", CityViewModel.calculateMaxPolluter(stations) );

        // comprobamos si le metemos un nulo
        assertNull( CityViewModel.calculateMaxPolluter(null) );

        // comprobamos si le metemos una lista vacía
        assertNull( CityViewModel.calculateMaxPolluter( new ArrayList<Station>()));
    }
}
