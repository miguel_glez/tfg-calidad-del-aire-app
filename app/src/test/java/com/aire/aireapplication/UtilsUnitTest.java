package com.aire.aireapplication;

import com.aire.aireapplication.persistence.domain.City;
import com.aire.aireapplication.utils.CityComparator;
import com.aire.aireapplication.utils.PollutionStaticData;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Created by Miguel on 27/05/2018.
 */

public class UtilsUnitTest {

    /*
        TESTS DE LA CLASE PollutionStaticData
     */

    // U-001
    @Test
    public void getIndexForValue_test(){
        assertEquals(PollutionStaticData.getIndexForValue(-10), 0); // valores negativos -> 0 (null de int)
        assertEquals(PollutionStaticData.getIndexForValue(0), 0); // probaremos con valores límite
        assertEquals(PollutionStaticData.getIndexForValue(49), 0);
        assertEquals(PollutionStaticData.getIndexForValue(50), 1);
        assertEquals(PollutionStaticData.getIndexForValue(99), 1);
        assertEquals(PollutionStaticData.getIndexForValue(100), 2);
        assertEquals(PollutionStaticData.getIndexForValue(149), 2);
        assertEquals(PollutionStaticData.getIndexForValue(150), 3);
        assertEquals(PollutionStaticData.getIndexForValue(199), 3);
        assertEquals(PollutionStaticData.getIndexForValue(200),4);
        assertEquals(PollutionStaticData.getIndexForValue(299), 4);
        assertEquals(PollutionStaticData.getIndexForValue(300), 5);
        assertEquals(PollutionStaticData.getIndexForValue(10000), 5); // valor absurdamente alto
    }

    // U-002
    @Test
    public void getValueForIndex_test(){
        assertEquals(PollutionStaticData.getValueForIndex(0), 0);
        assertEquals(PollutionStaticData.getValueForIndex(1), 50);
        assertEquals(PollutionStaticData.getValueForIndex(2), 100);
        assertEquals(PollutionStaticData.getValueForIndex(3), 150);
        assertEquals(PollutionStaticData.getValueForIndex(4), 200);
        assertEquals(PollutionStaticData.getValueForIndex(5), 300);

        assertEquals(PollutionStaticData.getValueForIndex(-1), -1); // valores raros debe devolver -1
        assertEquals(PollutionStaticData.getValueForIndex(15102), -1);
    }

    // U-003
    @Test
    public void cityComparator_test(){
        ArrayList<City> cities = new ArrayList<>();

        City a = new City(0, "A", false);
        City b = new City( 0, "B", true);
        City c = new City( 0, "C", false);
        City d = new City( 0, "D", true);
        City e = new City (0, "E", false);

        // añadimos las ciudades al array de forma desordenada
        cities.add(a);
        cities.add(e);
        cities.add(d);
        cities.add(c);
        cities.add(b);

        // El orden del array deberia pasar a ser B, D, A, C, E
        String[] correctOrder = { "B", "D", "A", "C", "E"};
        Collections.sort( cities, CityComparator.getComparator());
        for (int i=0; i<cities.size(); i++){
            assertEquals(cities.get(i).getName(), correctOrder[i]);
        }
    }

}
